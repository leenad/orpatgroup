<?php
$target_dir = "/var/www/html/orpatgroup/dailyuser/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(isset($_POST["submit"]))
 {
 	if($imageFileType != "xls" && $imageFileType != "xlsx") 
 	{
      echo "Sorry, only xls & xlsx are allowed.";    
    }
    else
    {
 		if(file_exists($target_file))
		{
    		unlink($target_file); //existed file will be deleted

    		if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
    		{
                echo '<script type="text/javascript">'; 
				echo 'alert("File Has been successfully Replaced");'; 
				echo 'window.location.href = "dashboard.php";';
				echo '</script>';

        		//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been replaced.";
    		}
    		else
    		{
    			echo '<script type="text/javascript">'; 
				echo 'alert("Sorry, there was an error replacing your file.");'; 
				echo 'window.location.href = "dashboard.php";';
				echo '</script>';

        		//echo "Sorry, there was an error replacing your file.";
    		}
		}
		else
		{
  			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
  			{
  				echo '<script type="text/javascript">'; 
				echo 'alert("File Has been successfully Uploaded");'; 
				echo 'window.location.href = "dashboard.php";';
				echo '</script>';

  				//echo '<script language="javascript">';
				// echo 'alert("File Has been successfully Uploaded")';
				// echo '</script>';
				// header('Location: dashboard.php');
      			//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
  			}
  			else
  			{
  				echo '<script type="text/javascript">'; 
				echo 'alert("Sorry, there was an error uploading your file.");'; 
				echo 'window.location.href = "dashboard.php";';
				echo '</script>';

      			//echo "Sorry, there was an error uploading your file.";
  			}
		}
	}
}

?>
