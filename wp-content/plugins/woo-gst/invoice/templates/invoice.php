<?php
function render_invoice_html( $order ) {
	$order_data = $order->get_data();
	$billing_state = $order_data['billing']['state'];
	$tax_display = get_option('woogst_invoice_tax_display');
	$tax_display = ( !$tax_display || $tax_display == 'inherit' ) ? get_option('woocommerce_tax_display_cart') : $tax_display;
	$totals = $order->get_order_item_totals($tax_display);
	$symbol = get_woocommerce_currency_symbol();
	$invoice_title = ( get_option( 'gst_invoice_heading' ) ) ? get_option( 'gst_invoice_heading' ) : "TAX INVOICE" ;
	$show_itemised = get_option( 'show_itemised_tax_invoice', true );
	ob_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php _e( 'Invoice', 'woo-gst' ); ?></title>
	<style type="text/css"><?php gst_invoice_template_style(); ?></style>
</head>
<body class="">
	<img src="https://orpatgroup.com/wp-content/uploads/2019/11/ORPAT-LOGO.png" id="profile-img-tag" width="100px; text-align:left;" />
<h1 class="text-center"><?php echo $invoice_title; ?></h1>
<table class="head container">
	<tr>
		<td class="header">
		<?php if(get_option('gst_profile_pic')) : ?>
			<img src="https://orpatgroup.com/wp-content/uploads/2019/11/ORPAT-LOGO.png" id="profile-img-tag" width="100px" />
		<?php endif; ?>
		</td>
		
		<td class="shop-info">
		<h4 class="shop-name"><?php _e('Sold by', 'gst'); ?></h4>
		<?php if(get_option('gst_shop_name')) : ?>
			<div class="shop-name"><?php echo get_option('gst_shop_name', true);?></div>
		<?php endif; ?>	
		<?php if(get_option('gst_shop_address')) : ?>
			<div class="shop-address"><?php echo nl2br(get_option('gst_shop_address', true)) ;?></div>
		<?php endif; ?>
		<?php if ( $gstno = get_option('woocommerce_gstin_number') ) : ?>
		<div class="shop-gstin">
			<?php _e( 'GSTIN:', 'woo-gst' ); ?> <?php echo $gstno; ?>
		</div>
		<?php endif; ?>
		</td>
	</tr>
</table>
<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<h4 class="address-title"><?php _e( 'Billing Details', 'gst' ); ?></h4>
			<?php echo $order->get_formatted_billing_address(); ?>
			<div class="billing-email"><?php echo $order_data['billing']['email']; ?></div>
			<div class="billing-phone"><?php echo $order_data['billing']['phone']; ?></div>
			<?php if( get_post_meta( $order->id, 'gstin_number', true ) ) : ?>
			<div class="billing-phone">
			<?php _e( 'GSTIN:', 'gst' ); ?>
			<?php echo get_post_meta( $order->get_id(), 'gstin_number', true ); ?>
			</div>
			<?php endif; ?>	
		</td>
		<td class="address shipping-address">
			<?php if ( $order->get_formatted_shipping_address() ) : ?>
			<h4 class="address-title"><?php _e( 'Shipping Details', 'gst' ); ?></h4>
			<?php echo $order->get_formatted_shipping_address(); ?>
			<?php endif; ?>
		</td>
		<td class="order-data">
			<table>		
				<tr class="order-number">
					<th><?php _e( 'Invoice Number:', 'woo-gst' ); ?></th>
					<td><?php echo $order_data['number']; ?></td>
				</tr>
				<tr class="order-number">
					<th><?php _e( 'Order Number:', 'woo-gst' ); ?></th>
					<td><?php echo $order_data['number']; ?></td>
				</tr>
				<tr class="order-date">
					<th><?php _e( 'Order Date:', 'woo-gst' ); ?></th>
					<td><?php echo date(' F j, Y', strtotime( $order->order_date ) ); ?></td>
				</tr>
				<tr class="payment-method">
					<th><?php _e( 'Payment Method:', 'woo-gst' ); ?></th>
					<td><?php echo $order_data['payment_method_title']; ?></td>
				</tr>
			</table>			
		</td>
	</tr>
</table>
<table class="order-details" style="display: none;" >
	<thead>
		<tr>
			<th class="product"><?php _e('Product', 'woo-gst' ); ?></th>
			<th class="quantity"><?php _e('Quantity', 'woo-gst' ); ?></th>
			<th class="price"><?php _e('Price', 'woo-gst' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php //print "<pre>"; 
			$taxes = $order->get_taxes();
			$arr_tax = array();
			
			foreach($taxes as $tax ){
				
				$tax_data = $tax->get_data();
				$price = $tax_data['tax_total'];
				$taxes_rate_id = $tax_data['rate_id'];
				$taxes_label = $tax_data['label'];
				$arr_tax[$taxes_rate_id] = array(
						'label' => $taxes_label,
						'cost' => $price
					);
			}

			//print_r($price);
		 ?>
		<?php $items = $order->get_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item ) : 

		$product_id = $item->get_data()['product_id'];
		// $product = new WC_Product($product_id);
		$product = $item->get_product();
		//print_r($item); print_r($product);die;
		?>
		<tr class="">
			<td class="product">
				<span class="item-name"><?php echo $product->get_name(); ?></span><br>
				<?php
				$txn = $item->get_data()['taxes']['total'];
				$price = "";
				foreach ($txn as $tax_key => $value) {
					if(!empty($value)){
						if (array_key_exists($tax_key,$arr_tax)){
							$value = number_format((float)$value, 2, '.', '');
							$price .= $symbol.$value . "(".$arr_tax[$tax_key]['label'].")" . "<br>";
						}
					}
				}
				?>
				<dl class="meta">
					<?php if( !empty( $product->get_sku() ) ) : ?>
						<dt class="sku"><?php _e( 'SKU:', 'woo-gst' ); ?></dt>
						<dd class="sku"><?php echo $product->get_sku()  ?></dd>
					<?php endif; ?>
					<?php if( $hsn = get_post_meta( $product_id, 'hsn_prod_id', true ) )  : ?>
						<dt class="hsn"><?php _e( 'HSN Code:', 'woo-gst' ); ?></dt>
						<dd class="hsn"><?php echo $hsn; ?></dd>
					<?php endif; ?>
				</dl>
			</td>
			<td class="quantity"><?php echo $item->get_data()['quantity']; ?></td>
			<td class="price" style="font-family: DejaVu Sans;">
				<?php
				switch ($tax_display) {
					case 'incl': echo wc_price( wc_get_price_including_tax(  $product ) ); break;
					case 'excl': echo wc_price( wc_get_price_excluding_tax(  $product ) ); break;
					default: echo wc_price( $product->get_price() ); break;
				}
				?>
				<?php if( !empty($price) && $show_itemised == "yes" ) echo "<br>" . $price; ?>
			</td>
		</tr>
		<?php endforeach; endif;  ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders">
				<div class="customer-notes">
				</div>				
			</td>
			<td class="no-borders" colspan="2">
				<table class="totals">
					<tfoot>
						<?php foreach( $totals as $key => $total ) : ?>
						<tr class="<?php echo $key; ?>">
							<td class="no-borders"></td>
							<th class="description"><?php echo $total['label']; ?></th>
							<td class="price" style="font-family: DejaVu Sans;"><span class="totals-price"><?php echo $total['value']; ?></span></td>
						</tr>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
	<?php  ?>
</table>


<!--manual table-->
<table class="order-details" style="font-size: 11px;">
	<thead>
	  <tr>
	    <!-- <th>S.No.</th> -->
		<th class="product" style="width:220px;"><?php _e('Product', 'woo-gst' ); ?></th>		
		<th class="quantity" style="width:45px;"><?php _e('Quantity', 'woo-gst' ); ?></th>
		<th class="price" style="width:5%;"><?php _e('Basic Price', 'woo-gst' ); ?></th>
		<th>Unit<br>Discount</th>
		<th>Taxable<br>Value</th>
		<?php if(strtolower($billing_state)=='gj'){ ?>
			<th>CGST<br>(Value | %)</th>
			<th>SGST<br>(Value | %)</th>
		<?php }else{ ?>
			<th>IGST<br>(Value | %)</th>
		<?php } ?>	
		<th>Total</th>
	  </tr>
	</thead>
	<tbody>  
		<?php 
			$taxes = $order->get_taxes();
			$arr_tax = array();
			
			foreach($taxes as $tax ){
				
				$tax_data = $tax->get_data();
				$price = $tax_data['tax_total'];
				$taxes_rate_id = $tax_data['rate_id'];
				$taxes_label = $tax_data['label'];
				$arr_tax[$taxes_rate_id] = array(
						'label' => $taxes_label,
						'cost' => $price
					);
			}
		 ?>
		<?php $items = $order->get_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item ) : 
		$product_id = $item->get_data()['product_id'];
		// $product = new WC_Product($product_id);
		$product = $item->get_product();
		?>
	  <tr>
	    <!-- <td>1</td> -->
	    <td><?php echo $product->get_name(); ?>
	    	<dl class="meta">
					<?php if( !empty( $product->get_sku() ) ) : ?>
						<dt class="sku"><?php _e( 'SKU:', 'woo-gst' ); ?></dt>
						<dd class="sku"><?php echo $product->get_sku()  ?></dd>
					<?php endif; ?>					
			</dl>
			<?php if( $hsn = get_post_meta( $product_id, 'hsn_prod_id', true ) )  : ?>
						<dt class="hsn"><?php _e( 'HSN ', 'woo-gst' ); ?></dt>
						<dd class="hsn"><?php echo $hsn; ?></dd>
					<?php endif; ?>
	    </td>
	    <?php
				$txn = $item->get_data()['taxes']['total'];
				$price = "";
				foreach ($txn as $tax_key => $value) {
					if(!empty($value)){
						if (array_key_exists($tax_key,$arr_tax)){
							$value = number_format((float)$value, 2, '.', '');
							$price .= $symbol.$value . "(".$arr_tax[$tax_key]['label'].")" . "<br>";
						}
					}
				}
		?>
	    
	    <td class="quantity"><?php echo $item->get_data()['quantity']; ?></td>
	    <td><?php
	    //var_dump($product);die;
				switch ($tax_display) {
					case 'incl': $sale_price =  wc_price( wc_get_price_including_tax(  $product ) ); break;
					case 'excl': $sale_price =   wc_price( wc_get_price_excluding_tax(  $product ) ); break;
					default: $sale_price =  wc_price( $product->get_price() ); break;
				}
				//$sale_price2 = $sale_price3= 0;
				$sale_price1 = str_replace('&#8377;', '', strip_tags($sale_price));
				$sale_price2 = str_replace(',', '', $sale_price1);
				// echo $sale_price3 = $sale_price1 + 0.00;
				// echo $sale_price3 = $sale_price1 +$sale_price3;die;

				// echo floatval($sale_price3, 2, '.', '');die;  // "1.00" (string)die;
				// //echo (int)number_format((float)$decimal, 2, '.', ''); // 1 (int)
				// echo (float)number_format((float)$sale_price3, 2, '.', '');
				// var_dump($sale_price3);die;
				echo $sale_price2;
				?></td>
	    <td>0</td>
	     <td><?php  echo ($sale_price2 * $item->get_data()['quantity']); ?></td>
	    <?php 
	    		 if( !empty($price) && $show_itemised == "yes" ){
	    		 	$TaxArr = explode("<br>", $price);
	    		 	$TaxArr = array_filter($TaxArr);
	    		 } 
	     if(strtolower($billing_state)=='gj' && count($TaxArr) >1){ ?>	     
			<td><?php echo (strip_tags(str_replace("&#8377;","", str_replace('(9% C-GST)', '', $TaxArr[0]) ))) ?> | 9.00</td>
			<td><?php echo (strip_tags(str_replace("&#8377;","", str_replace('(9% S-GST)', '', $TaxArr[1]) ))) ?> | 9.00</td>
			<?php }else { ?> 
			<td><?php echo (strip_tags(str_replace("&#8377;","", str_replace('(18% I-GST)', '', $TaxArr[0]) ))) ?> | 18.00</td>
			<?php } ?>
	    <td><?php echo ($product->price*$item->get_data()['quantity'])?></td>
	  </tr>
	  <?php endforeach; endif;  ?>
  	</tbody>
  	<tfoot style="width:100%;">
		<tr class="no-borders">
			<td class="no-borders" colspan="4">
				<div class="customer-notes">
				</div>				
			</td>
			<td class="no-borders" colspan="3">
				<table class="totals">
					<tfoot>
						<?php foreach( $totals as $key => $total ) : ?>
						<tr class="<?php echo $key; ?>">
							<td class="no-borders"></td>
							<th class="description"><?php echo $total['label']; ?></th>
							<td class="price" style="font-family: DejaVu Sans;"><span class="totals-price"><?php echo $total['value']; ?></span></td>
						</tr>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>



<?php if(get_option('gst_footer_conditions', true)) : ?>
<div id="footer">
	<?php echo get_option('gst_footer_conditions', true);?>
</div>
<?php endif; ?>
</body>
</html>
<?php
	return ob_get_clean();
}
function gst_invoice_template_style() {
	ob_start();
	include_once( 'style.css' );
	echo ob_get_clean();
}