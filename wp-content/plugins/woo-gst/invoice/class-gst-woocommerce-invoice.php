<?php

require_once "vendor/autoload.php";
require_once "templates/invoice.php";

use Dompdf\Dompdf;

if( ! class_exists( 'WooGstInvoice' ) ) :

	/**
	 * WooGstInvoice class
	 * Generates PDF Invoices
	 */

	class WooGstInvoice {
		
		function __construct() {

			/**
			 * Add PDF actio to the order columns
			 */
			add_action( 'woocommerce_admin_order_actions_end', array( $this, 'add_invoice_listing_actions' ) );
			
			/**
			 * Add css on admin side
			 */
			add_action( 'admin_enqueue_scripts', array( $this, 'gst_invoice_admin_scripts' ) );

			/**
			 * AJAX to get the PDF invoice of product
			 */
			add_action( 'wp_ajax_generate_woo_gst_pdf', array( $this, 'generate_pdf_invoice_ajax' ) );


			// add_action( 'init', array( $this, 'woo_gst_invoice_print_notices' ) );

			add_action('admin_menu' , array( $this, 'gst_pdf_invoice' ));


			add_filter( 'woocommerce_my_account_my_orders_actions', array($this, 'woo_gst_add_my_account_order_actions'), 10, 2 );

			add_action( 'add_meta_boxes', array($this, 'woo_gst_order_meta_boxes') );

		}

		/*start 31-07-2018*/

		public function gst_pdf_invoice() {
		    add_submenu_page( 'gst-settings', 'GST PDF Invoice', 'GST PDF Invoice', 'manage_options', 'gst-custom-submenu-page', array( $this, 'gst_pdf_invoice_callback'  )); 
		}

		/*end 31-07-2018*/
		
		public function gst_invoice_admin_scripts(){

			wp_enqueue_style( 'gst-pdf-invoice', gst_RELATIVE_PATH.'/css/admin.css' );
		}
		
		/**
		 * @hooked woocommerce_admin_order_actions_end
		 * @param $order [WC Object]
		 */
		public function add_invoice_listing_actions($order){

			if( $order->get_status() == 'completed' || $order->get_status() == 'processing' ){
				$listing_actions = array(
					'url'		=> wp_nonce_url( admin_url( "admin-ajax.php?action=generate_woo_gst_pdf&order_id=" . $order->get_id() ),'nonce', 'generate_woo_gst_pdf' ),
					'img'       => "<img src='".gst_RELATIVE_PATH ."/images/pdf.png' alt='invoice' />"
				);
				// echo "<a href='".$listing_actions['url']."' target='_blank'>PDF</a>";
				printf( "<a href='%s' title='PDF GST Invoice' class='button woo-gst-invoice' >%s</a>", esc_url( $listing_actions['url'] ),$listing_actions['img'] );
			}
		}

		/**
	     * woo_gst_add_my_account_order_actions
	     * Add the download PDF button on myaccount page
	     * @param $actions | array of actions
	     * @param $order | Object of the order
	     */
		public function woo_gst_add_my_account_order_actions( $actions, $order ) {
			if( $order->get_status() == 'completed' || $order->get_status() == 'processing' ){
				$btn_label = ( get_option('gst_ac_btn_name') ) ? get_option('gst_ac_btn_name') : "PDF Invoice";
				$actions['woo_gst_pdf_invoice'] = array(
            	// adjust URL as needed
					'url'  => wp_nonce_url( admin_url( "admin-ajax.php?action=generate_woo_gst_pdf&order_id=" . $order->get_id() ),'nonce', 'generate_woo_gst_pdf' ),
					'name' => $btn_label,
				);
			}

			return $actions;
		}


	    /**
	     * woo_gst_order_meta_boxes
	     * Add the meta boxes required
	     */
	    public function woo_gst_order_meta_boxes() {
	        add_meta_box( 'woo_gst_order_gstin', __('Customer GSTIN Number','woo-gst'), array($this, 'woo_gst_customer_gstin_number'), 'shop_order', 'side', 'core' );
	        add_meta_box( 'woo_gst_order_pdfinvoice', __('Order PDF Invoice','woo-gst'), array($this, 'woo_gst_pdf_invoice'), 'shop_order', 'side', 'core' );
	    }

	    /**
		 * woo_gst_customer_gstin_number
		 * Displays the meta box for customer GSTIN number
		 */
		public function woo_gst_customer_gstin_number(){
		    global $post;

		    $meta_field_data = get_post_meta( $post->ID, 'gstin_number', true );

		    echo '<input type="hidden" name="woo_gst_customer_gst_nonce" value="' . wp_create_nonce() . '">
		    <p><input type="text" name="gstin_number" placeholder="' . $meta_field_data . '" value="' . $meta_field_data . '"></p>';
		}

		/**
	     * woo_gst_pdf_invoice
	     * Displays the meta box for download PDF invoice button
	     */
	    public function woo_gst_pdf_invoice(){
	        global $post;
	        $order = wc_get_order( $post->ID );
	        if(empty($order)) {
	        	return;
	        }
	        //$order->get_status() == 'completed' || $order->get_status() == 'processing'
	        if(1){
	        	echo '<p><a href="'.wp_nonce_url( admin_url( "admin-ajax.php?action=generate_woo_gst_pdf&order_id=" . $post->ID ),'nonce', 'generate_woo_gst_pdf' ).'" class="button-primary">Download PDF Invoice</a></p>';
	        }
	    }

		/**
		 * @hooked wp_ajax_generate_wpo_gst_pdf
		 * Generates the PDF
		 */
		public function generate_pdf_invoice_ajax(){


			$page_url = admin_url( 'edit.php?post_type=shop_order' );
			
			//Redirect if order id is empty
			if ( ! isset( $_GET['order_id'] ) || empty( $_GET['order_id'] ) ) wp_safe_redirect( $page_url );

			$order = wc_get_order( $_GET['order_id'] );
			
			//Redirect if order data is empty
			if ( empty($order) ) wp_safe_redirect( $page_url );

			$html = render_invoice_html($order);

			$options = array('isRemoteEnabled' => true);
			$dompdf = new Dompdf($options);

			$dompdf->loadHtml($html);

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			// $output = $dompdf->output();
			$filename = 'invoice-'.$_GET['order_id'];
			$dompdf->stream($filename);
			// header('Content-Description: File Transfer');
			// header('Content-type: application/pdf');
			// header('Content-Disposition: inline; filename="'.$filename.'.pdf"');

			// echo $output;
			wp_die();
		}

		/**
		 * woo_gst_invoice_print_notices
		 * print notices
		 */
		public function woo_gst_invoice_print_notices() {
			add_action( 'admin_notices', function(){
				$class = 'notice notice-error';
				if ( isset( $_GET['wietype'] ) && $_GET['wietype'] == 'orderempty'  )
				$message = __( 'No order.', 'gst' );

				if ( isset( $_GET['wietype'] ) && $_GET['wietype'] == 'order'  )
				$message = __( 'Invalid Order Id.', 'gst' );

				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 
			} );

		}

		public function gst_pdf_invoice_callback() {

			if( ! get_option('gst_shop_name') ) {
				update_option( 'gst_shop_name', get_bloginfo( 'name' ) );
			}
			$invoice_title = ( get_option('gst_invoice_heading') ) ? get_option('gst_invoice_heading') : "TAX INVOICE";
			$btn_label = ( get_option('gst_ac_btn_name') ) ? get_option('gst_ac_btn_name') : "PDF Invoice";
			?>
			<h3><?php _e('GST PDF Invoice Page','gst'); ?></h3>
			<form method="POST" enctype="multipart/form-data">
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row"><?php _e('PDF invoice title','gst'); ?></th>
							<td><input type="text" id="invoice_heading" name="invoice_heading" value="<?php echo $invoice_title;?>" size="72" placeholder="" /></td>
						</tr>
						<tr>
							<th scope="row"><?php _e('Shop header/logo','gst'); ?></th>
							<td>					
								<input type="file" name="profile_pic" id="profile-img" value="" style="width: 100%">
								<?php if(get_option('gst_profile_pic')) : ?>
									<img src="<?php echo get_option('gst_profile_pic');?>" id="profile-img-tag" width="100px" />
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<th scope="row"><?php _e('Shop Name','gst'); ?></th>
							<td><input type="text" id="shop_name" name="shop_name" value="<?php echo get_option('gst_shop_name');?>" size="72" placeholder=""></td>
						</tr>
						<tr>
							<th scope="row"><?php _e('Shop Address','gst'); ?></th>
							<td>
								<textarea id="shop_address" name="shop_address" cols="72" rows="8" placeholder=""><?php echo get_option('gst_shop_address');?></textarea>
							</td>
						</tr>
						<tr>
							<th scope="row"><?php _e('Footer Note ( Example : terms &amp; conditions, policies, etc.)','gst'); ?></th>
							<td>
								<textarea id="footer" name="footer_conditions" cols="72" rows="4" placeholder=""><?php echo get_option('gst_footer_conditions');?></textarea>
							</td>
						</tr>
						<tr>
							<th scope="row"><?php _e('Download PDF invoice button label on My Orders section','gst'); ?></th>
							<td>
								<input type="text" id="my_ac_btn_name" name="ac_btn_name" value="<?php echo $btn_label;?>" size="72" placeholder="">
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit"><input type="submit" name="submit-woo-gst-setting" id="submit" class="button button-primary" value="Save Changes"></p>
			</form>
			<?php
		}
			
	}

	new WooGstInvoice();

endif;

