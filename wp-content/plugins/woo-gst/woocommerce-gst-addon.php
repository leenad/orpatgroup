<?php
/**
 * Plugin Name: WooCommerce GST PRO
 * Description: WooCommerce GST PRO addon.
 * Plugin URI: https://www.woocommercegst.co.in/
 * Author: Stark Digital
 * Author URI: http://starkdigital.net/
 * Version: 1.5.3
 * WC requires at least: 3.0.0
 * WC tested up to: 3.7.1
 */

if (!defined('ABSPATH'))
{
    exit; // Exit if accessed directly
}
require_once('inc/functions.php');
/**
 * Check WooCommerce exists
 */
if ( fn_is_woocommerce_active() ) {
	define('gst_RELATIVE_PATH', plugin_dir_url( __FILE__ ));
	define('gst_ABS_PATH', plugin_dir_path(__FILE__));
	define( 'gst_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

	define('GST_SERVER_URL', 'https://www.woocommercegst.co.in');
	define('GST_LICENSE_VERIFICATION_KEY', '5b67e199026bd7.95803195');
	define('GST_VERSION', '1.5');

	require_once( 'class-gst-woocommerce-addon.php' );

	// plugin update checker
	require_once 'plugin-update-checker/plugin-update-checker.php';
	$woo_gst_update_checker = Puc_v4_Factory::buildUpdateChecker(
		GST_SERVER_URL. '/plugin.json',
		__FILE__, //Full path to the main plugin file or functions.php.
		'woo-gst'
	);
	$gst_settings = new WC_GST_Settings();
	$gst_settings->init();

	/**
	 * woogst_activation_hook 
	 * @return [type] [description]
	 * @since 1.5.3
	 */
	function woogst_activation_hook() {
		$gst_settings = new WC_GST_Settings();
		$gst_settings->check_woo_gst_tax_slabs();
	}
	register_activation_hook( __FILE__, 'woogst_activation_hook' );
} else {
	add_action( 'admin_notices', 'fn_gst_admin_notice__error' );
}

/**
 * Include setting page JS
 */
add_action( 'admin_enqueue_scripts', 'gst_pdf_script' );
function gst_pdf_script($hook) {
	if ('woocommerce_page_gst-custom-submenu-page' != $hook) return;

	wp_enqueue_script( 'gst_script', plugin_dir_url( __FILE__ ) . 'js/custom.js' );
}

add_action( 'admin_enqueue_scripts', 'gst_multiselect_style_script' );

function gst_multiselect_style_script( $hook ) {
	if( isset($_GET['page']) && $_GET['page'] == 'wc-settings' && isset($_GET['tab']) && $_GET['tab'] == 'settings_gst_tab' ) :
	
		wp_enqueue_script( 'gst_chosen_script', plugin_dir_url( __FILE__ ) . 'js/chosen.jquery.min.js' );
		wp_enqueue_style( 'gst_chosen_style', plugin_dir_url( __FILE__ ) . 'css/chosen.min.css' );
	endif;
}

/**
 * Call a method which saves the settings
 */
add_action('init', 'save_woo_gst_settings');
function save_woo_gst_settings()
{
	if(isset($_POST['submit-woo-gst-setting'])) {
		$options = $_POST;
		if( isset( $_FILES["profile_pic"]["name"] ) && !empty( $_FILES["profile_pic"]["name"] ) ) :

			$upload = wp_upload_bits($_FILES["profile_pic"]["name"], null, file_get_contents($_FILES["profile_pic"]["tmp_name"]));
		$filename = $upload['url'];
		update_option( 'gst_profile_pic', $filename );
		endif;
		
		unset($_POST['submit-woo-gst-setting']);
		foreach ($options as $key => $value)
			update_option( 'gst_'.$key, trim($value));

	}
}

/**
 * Add css for Download PDF button on My Account page
 */
add_action( 'wp_enqueue_scripts', 'woo_gst_css' );
function woo_gst_css() {
	wp_enqueue_style( 'woo-gst-front',plugin_dir_url( __FILE__ ) . 'css/woo-gst.css' );
}


