<style type="text/css">
	.resize-vertical{
    resize: vertical !important;
}
.form-group {
    border-bottom: 1px solid #333333 !important;
    margin-bottom: 20px !important;
}
input, select, textarea {
    width: 100%;
    border: 0px !important;
}
.formstart{
    padding-top:30px;
}
.vc_single_image-wrapper.vc_box_border_grey {
    border: 20px solid gray;
    margin-bottom: 20px;
}
.entry-title h2{
        font-size: 40px;
    font-weight: 400;
    text-align: center;
    line-height: 50px;
    font-family: 'Josefin Sans', sans-serif;
}
.clients-slider .tp-bgimg.defaultimg {
       width: 99% !important;
    height: 99% !important;
    border: 2px solid #7f7f7f;

}
.formpage form {
    width: 55%;
    margin: 0 auto;
}
</style>

<?php
/* Template Name: form-refund */ 
include("/var/www/html/orpatgroup/wp-config.php");
function sendSMS($mobile){
		//Your authentication key
		$authKey = "299535ABPW0525L735da95471";

		//Multiple mobiles numbers separated by comma
		$mobileNumber = $mobile;

		//Sender ID,While using route4 sender id should be 6 characters long.
		$senderId = "ORPATG";

		//Your message to send, Add URL encoding here.
		$message = urlencode("Thank you for sharing your details with Orpat Group. Our executive will connect with you shortly. For product details, click bit.ly/2mHMI03 or call 99136 95616.");

		//Define route 
		$route = "4";
		//Prepare you post parameters
		$postData = array(
		    'authkey' => $authKey,
		    'mobiles' => $mobileNumber,
		    'message' => $message,
		    'sender' => $senderId,
		    'route' => $route
		);

		//API URL
		$url="http://api.msg91.com/api/sendhttp.php";

		// init the resource
		$ch = curl_init();
		curl_setopt_array($ch, array(
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => $postData
		    //,CURLOPT_FOLLOWLOCATION => true
		));


		//Ignore SSL certificate verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


		//get response
		$output = curl_exec($ch);

		//Print error if any
		if(curl_errno($ch))
		{
		    echo 'error:' . curl_error($ch);
		}

		curl_close($ch);

		//echo $output;
}

function SaveImageFolder($upload_input_name, $image_upload_path){
	$types = array('image/png' => 'png', 'image/jpg' => 'jpg', 'image/jpeg' => 'jpeg');
	$error=false;
	$extension=array("png","jpg","jpeg");
	//$images_arr = [];
    foreach($_FILES[$upload_input_name]["tmp_name"] as $key=>$tmp_name) {
    	//echo "leea";
        $old_file_name=$_FILES[$upload_input_name]["name"][$key];
        $file_tmp=$_FILES[$upload_input_name]["tmp_name"][$key];
        $ext=pathinfo($file_name,PATHINFO_EXTENSION);
        //$video_file = $_FILES[$upload_input_name]['name'][$key];
        if(array_key_exists($_FILES[$upload_input_name]['type'][$key], $types)){
			$new_file_name = md5(microtime().'#'.rand()).'.'.$types[$_FILES[$upload_input_name]['type'][$key]];
			$new_file_name_path = $image_upload_path.$new_file_name;

			if(!file_exists($new_file_name_path)){

				move_uploaded_file($_FILES[$upload_input_name]['tmp_name'][$key], $new_file_name_path);
			}else{

				$new_file_name = md5(microtime().'#'.time()).'.'.$types[$_FILES[$upload_input_name]['type'][$key]];
				$new_file_name_path = $image_upload_path.$new_file_name;
				move_uploaded_file($_FILES[$upload_input_name]['tmp_name'][$key], $new_file_name_path);
			}
			$images_arr[] =$new_file_name; 		

		}
	}
	return implode(',',$images_arr);
}

if($_POST['post_refund'] && $_FILES['refund_pic'] != ""){
	 //print_r($_POST);print_r($_FILES);die;
	 extract($_POST);
	 global $wpdb;
	 $created_at = date('Y-m-d H:i:s');
	 $updated_at = date('Y-m-d H:i:s');
	 $images_json = SaveImageFolder('refund_pic', '/var/www/html/orpatgroup/wp-content/uploads/refund_images/');
	 //print_r($images_json);die;

	  $sql ="INSERT INTO `orpat_admin_panel`.`refund_request`(`order_id`, `return_reason`, `images`, `created_at`, `updated_at`) VALUES ('".$refund_orderid."', '".$refund_reason."', '".$images_json."', '".$created_at."', '".$updated_at."')";

 	if($wpdb->query($sql))
 	{
 	//	echo "executed";
 		//SEND EMAIL
 		$json_decoded_images = explode(',', $images_json);
 		$images_html ="";
 		foreach ($json_decoded_images as $ki => $vi) {
 			$images_html .= "<br/><img src ='https://orpatgroup.com/wp-content/uploads/refund_images/".$vi."'  width='100px' height='100px' /><br/>"; 
 		} 
 		//echo $images_html;die;
	    $subject="Refund for Orpat Group product";

        $body = "Form Details are: <br><br>
		        Order Id - ".$refund_orderid."<br>
				Return Reason - ".$refund_reason."<br>
				Images - ".$images_html."<br>";
        $body .="<br>Thank you.";
        $sender_email = "assist@firsteconomy.com";
        $sender_password = "f3e6f3e6";
        $reciever ="<distributor@orpatgroup.com>, <jigar@firsteconomy.com>, <marketing1@orpatgroup.com>, <amiti@firsteconomy.com>";
          
	    $headers = 'MIME-Version: 1.0'."\r\n";
	    $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
	    $headers .= "From: Orpatgroup - <marketing@orpatgroup.com>"; 
	    mail($reciever, $subject, $body, $headers); 
	    //SEND EMAIL
	    
 		//sendSMS($corporategiftform_mobile);
 		unset($_POST);
 		header("Location: https://orpatgroup.com/refund/");
 		exit(0);die;
 	}
 	else
 	{
 	//	echo "not executed";
 	}	
}
get_header();
?>
<div id="main-content" class="main-content formpage home-page <?php echo esc_attr(tmpmela_sidebar_position()); ?> <?php echo esc_attr(tmpmela_page_layout()); ?> ">
  <?php
	if ( is_front_page() && tmpmela_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
<?php if (get_option('tmpmela_page_sidebar') == 'yes') : ?>
<div id="primary" class="content-area">

	<div class="form-outer">
		<div class="text">
			<h3>To return your Orpat product fill the below details:</h3><br>
		</div>

<form id="refund" name="refund" action="https://orpatgroup.com/refund/" method="POST" enctype="multipart/form-data">
   Fields marked with an * are required
   
   <div class="form-group"><label for="refund_orderid">Order ID *</label><br>
      <input id="orderid" name="refund_orderid" class="form-control" required="" type="number" placeholder="" />
   </div>
   
   <div class="form-group"><label for="refund_reason">Reason for returning product *</label><br>
      <input id="reason" name="refund_reason" class="form-control" required="" type="text" placeholder="" />
   </div>
   <div class="form-group"><label for="refund_pic">Upload defective product pics *</label><br>
   		<input id="pics" type="file" name="refund_pic[]" accept="image/*" required="" multiple="">
   	</div>
   
   <input type="submit" value="Submit" name="post_refund" />
</form>
<span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span>
	</div>

<?php else : ?>
<div id="primary" class="main-content-inner-full">
<?php endif; ?> 
    <div id="content" class="site-content" role="main">
      <?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
					// Include the page content template.
					get_template_part( 'content', 'page' ); ?>
      <?php endwhile;
			?>
    </div><!-- #content -->
</div><!-- #primary -->
   <?php 
if (get_option('tmpmela_page_sidebar') == 'yes') : 
	get_sidebar( 'content' );
	get_sidebar();
endif;  ?><!-- #main-content -->
</div>
<script type="text/javascript">
	function corporate_gifting_submit(){
		var type = 1;
		var name = jQuery('#name').val();		
		var email = jQuery('#email').val();
		var phone = jQuery('#phone').val();
		var company = jQuery('#company').val();
		var quantity = jQuery('#quantity').val();
		var custmizationreq = jQuery('#custmizationreq').val();
		var comment = jQuery('#comment').val();
		var pickgift = jQuery('#pickgift').val();
		var ga_campaign = jQuery('#ga_campaign').val();
		var ga_medium = jQuery('#ga_medium').val();
		var ga_campaign = jQuery('#ga_campaign').val();
	jQuery.ajax({
		url : "http://panel.orpatgroup.com/api/institutionalSale",
		method: 'POST',
		data : {
			 "name": name,
			 "email": email,
		 "mobile": phone,
		 "quantity": quantity,
		 "company" :company,		 
		 "custmizationreq": custmizationreq,
		 "comment": comment,
		 "pickgift" : pickgift,
		 "ga_campaign" : ga_campaign ,
		 "ga_medium" : ga_medium,
		 "ga_campaign" :ga_campaign
		},
		success : function(data){
			console.log(data);
		}
	});
}
</script>
<?php get_footer(); ?>