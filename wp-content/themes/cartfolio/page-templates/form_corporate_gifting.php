<?php
/* Template Name: form-corporate-gifting */ 
include("/var/www/html/orpatgroup/wp-config.php");
function sendSMS($mobile){
		//Your authentication key
		$authKey = "299535ABPW0525L735da95471";

		//Multiple mobiles numbers separated by comma
		$mobileNumber = $mobile;

		//Sender ID,While using route4 sender id should be 6 characters long.
		$senderId = "ORPATG";

		//Your message to send, Add URL encoding here.
		$message = urlencode("Thank you for sharing your details with Orpat Group. Our executive will connect with you shortly. For product details, click bit.ly/2mHMI03 or call 99136 95616.");

		//Define route 
		$route = "4";
		//Prepare you post parameters
		$postData = array(
		    'authkey' => $authKey,
		    'mobiles' => $mobileNumber,
		    'message' => $message,
		    'sender' => $senderId,
		    'route' => $route
		);

		//API URL
		$url="http://api.msg91.com/api/sendhttp.php";

		// init the resource
		$ch = curl_init();
		curl_setopt_array($ch, array(
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => $postData
		    //,CURLOPT_FOLLOWLOCATION => true
		));


		//Ignore SSL certificate verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


		//get response
		$output = curl_exec($ch);

		//Print error if any
		if(curl_errno($ch))
		{
		    echo 'error:' . curl_error($ch);
		}

		curl_close($ch);

		//echo $output;
}
if($_POST['corpgift']){
	 
	 extract($_POST);
	 global $wpdb;
	 $created_at = date('Y-m-d H:i:s');

	 $sql ="INSERT INTO `orpat_admin_panel`.`institutional_sales`(  `name`, `mobile`,  `email`, `company_name`, `ga_source`, `ga_medium`, `ga_campaign`, `pickgift`, `custmizationreq`, `comment`, `quantity`) VALUES ('".$corporategiftform_name."', '".$corporategiftform_mobile."', '".$corporategiftform_email."', '".$corporategiftform_company."', '".$ga_source."', '".$ga_medium."', '".$ga_campaign."', '".$corporategiftform_pickgift."', '".$corporategiftform_custmizationreq."', '".$corporategiftform_comment."', '".$corporategiftform_quantity."')";
 	if($wpdb->query($sql))
 	{
 	//	echo "executed";
 		//SEND EMAIL
	    $subject="Corporate Gifting with Orpat Group";

        $body = "Form Details are: <br><br>
		        Name - ".$corporategiftform_name."<br>
				Mobile - ".$corporategiftform_mobile."<br>
				Email - ".$corporategiftform_email."<br>
				Quantity - ".$corporategiftform_quantity."<br>
				Company - ".$corporategiftform_company."<br>
				Pick Gift - ".$corporategiftform_pickgift."<br>				
				Customization required - ".$corporategiftform_custmizationreq."<br>
				Comment - ".$corporategiftform_comment."<br>";
        $body .="<br>Thank you.";
        $sender_email = "assist@firsteconomy.com";
        $sender_password = "f3e6f3e6";
        $reciever ="<distributor@orpatgroup.com>, <jigar@firsteconomy.com>, <marketing1@orpatgroup.com>, <amiti@firsteconomy.com>";
          
	    $headers = 'MIME-Version: 1.0'."\r\n";
	    $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
	    $headers .= "From: Orpatgroup - <marketing@orpatgroup.com>"; 
	     mail($reciever, $subject, $body, $headers); 
	    //SEND EMAIL
	    
 		sendSMS($corporategiftform_mobile);
 		unset($_POST);
 		header("Location: https://orpatgroup.com/corporate-gifting-thankyou-page/");
 		exit(0);die;
 	}
 	else
 	{
 	//	echo "not executed";
 	}	
}
get_header();
?>
<div id="main-content" class="main-content formpage home-page <?php echo esc_attr(tmpmela_sidebar_position()); ?> <?php echo esc_attr(tmpmela_page_layout()); ?> ">
  <?php
	if ( is_front_page() && tmpmela_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
<?php if (get_option('tmpmela_page_sidebar') == 'yes') : ?>
<div id="primary" class="content-area">

	<div class="form-outer">
		<div class="text">
			<h3>Corporate Gifting from the House of Orpat</h3>
Gifts are the best way to make the moments special, be it a regular day, a special meeting with your colleagues, a conference or a festival. We, as a brand, strive to make it as memorable as possible through our extensive range of home appliance and office-use products. So, prepare yourself for the festive season and gift your employees & business associates to make their celebration even more exciting. Get the best rates with a quick supply of the products in time. Choose from our wide range of products, customize it with the occasion and gift to please them.
		</div>

		<h2 class="formstart" style="text-align: center;">Pick Your Gift</h2>
<p style="text-align: center;">Pick any gift from our range of products and customise it as per the occasion.</p>

<form id="corporate_gifting" name="corporate_gifting" action="https://orpatgroup.com/corporate-gifting/" method="POST">
   Fields marked with an * are required
   <div class="form-group">
      <label for="pickgift">Pick Your Gift *</label>
      <select id="pickgift" class="" name="corporategiftform_pickgift" required="" aria-invalid="false" aria-describedby="">
         <option selected="selected" value="AjantaPopularClocks">Ajanta Popular Clocks</option>
         <option value="OrpatTimepieces">Orpat Timepieces</option>
         <option value="OrpatCalculators">Orpat Calculators</option>
         <option value="OrpatEducationalToys">Orpat Educational Toys</option>
         <option value="OrpatFans">Orpat Fans</option>
         <option value="OrpatHomeAppliances">Orpat Home Appliances</option>
         <option value="OrpatTelephones">Orpat Telephones</option>
      </select>
   </div>
   <div class="form-group"><label for="addqty">Add Quantity *</label>
      <input id="quantity" name="corporategiftform_quantity" class="form-control" required="" type="number" placeholder="" />
   </div>
   <div class="form-group">
      <label for="custmizationreq">Customization Required *</label>
      <select id="custmizationreq" class="" name="corporategiftform_custmizationreq" required="" aria-invalid="false" aria-describedby="">
         <option selected="selected" value="yes">Yes</option>
         <option value="no">No</option>
      </select>
   </div>
   <div class="">
      <h2 style="text-align: center;">Fill in the Gifting Details</h2>
   </div>
   <div class="form-group"><label for="name">Name *</label>
      <input id="name" name="corporategiftform_name" class="form-control" required="" type="text" placeholder="" />
   </div>
   <div class="form-group"><label for="email">Email *</label>
      <input id="email" name="corporategiftform_email" class="form-control" required="" type="email" placeholder="" />
   </div>
   <div class="form-group"><label for="compname">Company Name *</label>
      <input id="company" name="corporategiftform_company" class="form-control" required="" type="text" placeholder="" />
   </div>
   <div class="form-group"><label for="contactno">Contact No *</label>
      <input id="phone" name="corporategiftform_mobile" class="form-control" required="" type="number" placeholder="" />
   </div>
   <input type="hidden" name="ga_source" id="ga_source" value="<?php echo $_GET['utm_source'];?>">
   <input type="hidden" name="ga_medium" id="ga_medium" value="<?php echo $_GET['utm_medium'];?>">
   <input type="hidden" name="ga_campaign" id="ga_campaign" value="<?php echo $_GET['utm_campaign'];?>">
   <div class="form-group"><label for="details">Any more specific details</label>
      <textarea id="comment" name="corporategiftform_comment" class="resize-vertical" name="" aria-invalid="false" aria-describedby=""></textarea>
   </div>
   <input type="submit" value="Request A Call Back" name="corpgift" />
</form>
<span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span>
	</div>

<?php else : ?>
<div id="primary" class="main-content-inner-full">
<?php endif; ?> 
    <div id="content" class="site-content" role="main">
      <?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
					// Include the page content template.
					get_template_part( 'content', 'page' ); ?>
      <?php endwhile;
			?>
    </div><!-- #content -->
</div><!-- #primary -->
   <?php 
if (get_option('tmpmela_page_sidebar') == 'yes') : 
	get_sidebar( 'content' );
	get_sidebar();
endif;  ?><!-- #main-content -->
</div>
<script type="text/javascript">
	function corporate_gifting_submit(){
		var type = 1;
		var name = jQuery('#name').val();		
		var email = jQuery('#email').val();
		var phone = jQuery('#phone').val();
		var company = jQuery('#company').val();
		var quantity = jQuery('#quantity').val();
		var custmizationreq = jQuery('#custmizationreq').val();
		var comment = jQuery('#comment').val();
		var pickgift = jQuery('#pickgift').val();
		var ga_campaign = jQuery('#ga_campaign').val();
		var ga_medium = jQuery('#ga_medium').val();
		var ga_campaign = jQuery('#ga_campaign').val();
	jQuery.ajax({
		url : "http://panel.orpatgroup.com/api/institutionalSale",
		method: 'POST',
		data : {
			 "name": name,
			 "email": email,
		 "mobile": phone,
		 "quantity": quantity,
		 "company" :company,		 
		 "custmizationreq": custmizationreq,
		 "comment": comment,
		 "pickgift" : pickgift,
		 "ga_campaign" : ga_campaign ,
		 "ga_medium" : ga_medium,
		 "ga_campaign" :ga_campaign
		},
		success : function(data){
			console.log(data);
		}
	});
}
</script>
<?php get_footer(); ?>