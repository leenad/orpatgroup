<?php
/* Template Name: form-international-distributorship */ 
include("/var/www/html/orpatgroup/wp-config.php");
function sendSMS($mobile){
      //Your authentication key
      $authKey = "299535ABPW0525L735da95471";

      //Multiple mobiles numbers separated by comma
      $mobileNumber = $mobile;

      //Sender ID,While using route4 sender id should be 6 characters long.
      $senderId = "ORPATG";

      //Your message to send, Add URL encoding here.
      $message = urlencode("Thank you for sharing your details with Orpat Group. Our executive will connect with you shortly. For product details, click bit.ly/2mHMI03 or call 99136 95616.");

      //Define route 
      $route = "4";
      //Prepare you post parameters
      $postData = array(
          'authkey' => $authKey,
          'mobiles' => $mobileNumber,
          'message' => $message,
          'sender' => $senderId,
          'route' => $route
      );

      //API URL
      $url="http://api.msg91.com/api/sendhttp.php";

      // init the resource
      $ch = curl_init();
      curl_setopt_array($ch, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_POST => true,
          CURLOPT_POSTFIELDS => $postData
          //,CURLOPT_FOLLOWLOCATION => true
      ));


      //Ignore SSL certificate verification
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


      //get response
      $output = curl_exec($ch);

      //Print error if any
      if(curl_errno($ch))
      {
          echo 'error:' . curl_error($ch);
      }

      curl_close($ch);

      //echo $output;
}
if($_POST['interdist']){
	 
	 extract($_POST);
	 global $wpdb;
	 $created_at = date('Y-m-d H:i:s');

	 $sql ="INSERT INTO `orpat_admin_panel`.`international_distributorships`(  `name`, `mobile`, `email`, `country`, `state`, `compfortable_with_english`, `ga_source`, `ga_medium`, `ga_campaign`, `address`, `product`, `comment`) VALUES ('".$interdistiform_name."', '".$interdistiform_mobile."', '".$interdistiform_email."', '".$interdistiform_country."', '".$interdistiform_state."','".$interdistiform_compfortable_with_english."','".$ga_source."', '".$ga_medium."', '".$ga_campaign."', '".$interdistiform_address."', '".$interdistiform_product."', '".$interdistiform_comment."')";
 	if($wpdb->query($sql))
 	{
 	//	echo "executed";
      //SEND EMAIL
       $subject="International Distributorship with Orpat Group";
       $body = "Form Details are: <br><br>
              Name - ".$interdistiform_name."<br>
            Mobile - ".$interdistiform_mobile."<br>
            Email - ".$interdistiform_email."<br>
            Country - ".$interdistiform_country."<br>
            State - ".$interdistiform_state."<br>            
            Address - ".$interdistiform_address."<br>
            Comfotable with English - ".$interdistiform_compfortable_with_english."<br>
            Product - ".$interdistiform_product."<br>
            Comment - ".$interdistiform_comment."<br>";
        $body .="<br>Thank you.";
        $sender_email = "assist@firsteconomy.com";
        $sender_password = "f3e6f3e6";
        $reciever ="<distributor@orpatgroup.com>, <jigar@firsteconomy.com>, <marketing1@orpatgroup.com>, <amiti@firsteconomy.com>";
          
       $headers = 'MIME-Version: 1.0'."\r\n";
       $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
       $headers .= "From: Orpatgroup - <marketing@orpatgroup.com>"; 
        mail($reciever, $subject, $body, $headers); 
       
       //SEND EMAIL

      sendSMS($interdistiform_mobile);
 		unset($_POST);
      header("Location: https://orpatgroup.com/international-distributorship-thankyou-page/");
      exit(0);die;
 	}
 	else
 	{
 	//	echo "not executed";
 	}	
} 
get_header();
?>
<div id="main-content" class="main-content formpage home-page <?php echo esc_attr(tmpmela_sidebar_position()); ?> <?php echo esc_attr(tmpmela_page_layout()); ?> ">
  <?php
	if ( is_front_page() && tmpmela_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
<?php if (get_option('tmpmela_page_sidebar') == 'yes') : ?>
<div id="primary" class="content-area">

	<div class="form-outer">
		<div class="text">
			With an extensive range of home appliances and office-use products, Orpat Group takes pride in being the world’s largest clock manufacturer and India’s largest calculator manufacturer.<br>
Along with providing prompt service to our customers, we also offer efficient technical support and logistics to our distributors and actively participate in offers for them. Become a distributor with Orpat Group and earn great returns.<br>

Fill in your details to get started with your distributorship process.
		</div>

		<form id="intdistributorship" class="formstart" name="intdistributorship" method="POST" action="https://orpatgroup.com/international-distributorship/">
   Fields marked with an * are required
   <div class="form-group"><label for="name">Name *</label>
      <input id="name" class="form-control" required="" type="text" placeholder="" name="interdistiform_name" />
   </div>
   <div class="form-group">
      <label for="selectcountry">Select Country *</label>
      <select id="country" class="" name="interdistiform_country" required="" aria-invalid="false" aria-describedby="">
         <option value="AF">Afghanistan</option>
         <option value="AL">Albania</option>
         <option value="DZ">Algeria</option>
         <option value="AS">American Samoa</option>
         <option value="AD">Andorra</option>
         <option value="AO">Angola</option>
         <option value="AI">Anguilla</option>
         <option value="AQ">Antarctica</option>
         <option value="AG">Antigua And Barbuda</option>
         <option value="AR">Argentina</option>
         <option value="AM">Armenia</option>
         <option value="AW">Aruba</option>
         <option value="AU">Australia</option>
         <option value="AT">Austria</option>
         <option value="AZ">Azerbaijan</option>
         <option value="BS">Bahamas</option>
         <option value="BH">Bahrain</option>
         <option value="BD">Bangladesh</option>
         <option value="BB">Barbados</option>
         <option value="BY">Belarus</option>
         <option value="BE">Belgium</option>
         <option value="BZ">Belize</option>
         <option value="BJ">Benin</option>
         <option value="BM">Bermuda</option>
         <option value="BT">Bhutan</option>
         <option value="BO">Bolivia</option>
         <option value="BA">Bosnia And Herzegowina</option>
         <option value="BW">Botswana</option>
         <option value="BV">Bouvet Island</option>
         <option value="BR">Brazil</option>
         <option value="IO">British Indian Ocean Territory</option>
         <option value="BN">Brunei Darussalam</option>
         <option value="BG">Bulgaria</option>
         <option value="BF">Burkina Faso</option>
         <option value="BI">Burundi</option>
         <option value="KH">Cambodia</option>
         <option value="CM">Cameroon</option>
         <option value="CA">Canada</option>
         <option value="CV">Cape Verde</option>
         <option value="KY">Cayman Islands</option>
         <option value="CF">Central African Republic</option>
         <option value="TD">Chad</option>
         <option value="CL">Chile</option>
         <option value="CN">China</option>
         <option value="CX">Christmas Island</option>
         <option value="CC">Cocos (Keeling) Islands</option>
         <option value="CO">Colombia</option>
         <option value="KM">Comoros</option>
         <option value="CG">Congo</option>
         <option value="CD">Congo, The Democratic Republic Of The</option>
         <option value="CK">Cook Islands</option>
         <option value="CR">Costa Rica</option>
         <option value="CI">Cote D'Ivoire</option>
         <option value="HR">Croatia (Local Name: Hrvatska)</option>
         <option value="CU">Cuba</option>
         <option value="CY">Cyprus</option>
         <option value="CZ">Czech Republic</option>
         <option value="DK">Denmark</option>
         <option value="DJ">Djibouti</option>
         <option value="DM">Dominica</option>
         <option value="DO">Dominican Republic</option>
         <option value="EC">Ecuador</option>
         <option value="EG">Egypt</option>
         <option value="SV">El Salvador</option>
         <option value="GQ">Equatorial Guinea</option>
         <option value="ER">Eritrea</option>
         <option value="EE">Estonia</option>
         <option value="ET">Ethiopia</option>
         <option value="FK">Falkland Islands (Malvinas)</option>
         <option value="FO">Faroe Islands</option>
         <option value="FJ">Fiji</option>
         <option value="FI">Finland</option>
         <option value="FR">France</option>
         <option value="FX">France, Metropolitan</option>
         <option value="GF">French Guiana</option>
         <option value="PF">French Polynesia</option>
         <option value="TF">French Southern Territories</option>
         <option value="GA">Gabon</option>
         <option value="GM">Gambia</option>
         <option value="GE">Georgia</option>
         <option value="DE">Germany</option>
         <option value="GH">Ghana</option>
         <option value="GI">Gibraltar</option>
         <option value="GR">Greece</option>
         <option value="GL">Greenland</option>
         <option value="GD">Grenada</option>
         <option value="GP">Guadeloupe</option>
         <option value="GU">Guam</option>
         <option value="GT">Guatemala</option>
         <option value="GN">Guinea</option>
         <option value="GW">Guinea-Bissau</option>
         <option value="GY">Guyana</option>
         <option value="HT">Haiti</option>
         <option value="HM">Heard And Mc Donald Islands</option>
         <option value="VA">Holy See (Vatican City State)</option>
         <option value="HN">Honduras</option>
         <option value="HK">Hong Kong</option>
         <option value="HU">Hungary</option>
         <option value="IS">Iceland</option>
         <option selected="selected" value="IN">India</option>
         <option value="ID">Indonesia</option>
         <option value="IR">Iran (Islamic Republic Of)</option>
         <option value="IQ">Iraq</option>
         <option value="IE">Ireland</option>
         <option value="IL">Israel</option>
         <option value="IT">Italy</option>
         <option value="JM">Jamaica</option>
         <option value="JP">Japan</option>
         <option value="JO">Jordan</option>
         <option value="KZ">Kazakhstan</option>
         <option value="KE">Kenya</option>
         <option value="KI">Kiribati</option>
         <option value="KP">Korea, Democratic People's Republic Of</option>
         <option value="KR">Korea, Republic Of</option>
         <option value="KW">Kuwait</option>
         <option value="KG">Kyrgyzstan</option>
         <option value="LA">Lao People's Democratic Republic</option>
         <option value="LV">Latvia</option>
         <option value="LB">Lebanon</option>
         <option value="LS">Lesotho</option>
         <option value="LR">Liberia</option>
         <option value="LY">Libyan Arab Jamahiriya</option>
         <option value="LI">Liechtenstein</option>
         <option value="LT">Lithuania</option>
         <option value="LU">Luxembourg</option>
         <option value="MO">Macau</option>
         <option value="MK">Macedonia, Former Yugoslav Republic Of</option>
         <option value="MG">Madagascar</option>
         <option value="MW">Malawi</option>
         <option value="MY">Malaysia</option>
         <option value="MV">Maldives</option>
         <option value="ML">Mali</option>
         <option value="MT">Malta</option>
         <option value="MH">Marshall Islands</option>
         <option value="MQ">Martinique</option>
         <option value="MR">Mauritania</option>
         <option value="MU">Mauritius</option>
         <option value="YT">Mayotte</option>
         <option value="MX">Mexico</option>
         <option value="FM">Micronesia, Federated States Of</option>
         <option value="MD">Moldova, Republic Of</option>
         <option value="MC">Monaco</option>
         <option value="MN">Mongolia</option>
         <option value="ME">Montenegro</option>
         <option value="MS">Montserrat</option>
         <option value="MA">Morocco</option>
         <option value="MZ">Mozambique</option>
         <option value="MM">Myanmar</option>
         <option value="NA">Namibia</option>
         <option value="NR">Nauru</option>
         <option value="NP">Nepal</option>
         <option value="NL">Netherlands</option>
         <option value="AN">Netherlands Antilles</option>
         <option value="NC">New Caledonia</option>
         <option value="NZ">New Zealand</option>
         <option value="NI">Nicaragua</option>
         <option value="NE">Niger</option>
         <option value="NG">Nigeria</option>
         <option value="NU">Niue</option>
         <option value="NF">Norfolk Island</option>
         <option value="MP">Northern Mariana Islands</option>
         <option value="NO">Norway</option>
         <option value="OM">Oman</option>
         <option value="PK">Pakistan</option>
         <option value="PW">Palau</option>
         <option value="PA">Panama</option>
         <option value="PG">Papua New Guinea</option>
         <option value="PY">Paraguay</option>
         <option value="PE">Peru</option>
         <option value="PH">Philippines</option>
         <option value="PN">Pitcairn</option>
         <option value="PL">Poland</option>
         <option value="PT">Portugal</option>
         <option value="PR">Puerto Rico</option>
         <option value="QA">Qatar</option>
         <option value="RE">Reunion</option>
         <option value="RO">Romania</option>
         <option value="RU">Russian Federation</option>
         <option value="RW">Rwanda</option>
         <option value="KN">Saint Kitts And Nevis</option>
         <option value="LC">Saint Lucia</option>
         <option value="VC">Saint Vincent And The Grenadines</option>
         <option value="WS">Samoa</option>
         <option value="SM">San Marino</option>
         <option value="ST">Sao Tome And Principe</option>
         <option value="SA">Saudi Arabia</option>
         <option value="SN">Senegal</option>
         <option value="RS">Serbia</option>
         <option value="SC">Seychelles</option>
         <option value="SL">Sierra Leone</option>
         <option value="SG">Singapore</option>
         <option value="SK">Slovakia (Slovak Republic)</option>
         <option value="SI">Slovenia</option>
         <option value="SB">Solomon Islands</option>
         <option value="SO">Somalia</option>
         <option value="ZA">South Africa</option>
         <option value="GS">South Georgia, South Sandwich Islands</option>
         <option value="ES">Spain</option>
         <option value="LK">Sri Lanka</option>
         <option value="SH">St. Helena</option>
         <option value="PM">St. Pierre And Miquelon</option>
         <option value="SD">Sudan</option>
         <option value="SR">Suriname</option>
         <option value="SJ">Svalbard And Jan Mayen Islands</option>
         <option value="SZ">Swaziland</option>
         <option value="SE">Sweden</option>
         <option value="CH">Switzerland</option>
         <option value="SY">Syrian Arab Republic</option>
         <option value="TW">Taiwan</option>
         <option value="TJ">Tajikistan</option>
         <option value="TZ">Tanzania, United Republic Of</option>
         <option value="TH">Thailand</option>
         <option value="TL">Timor-Leste (East Timor)</option>
         <option value="TG">Togo</option>
         <option value="TK">Tokelau</option>
         <option value="TO">Tonga</option>
         <option value="TT">Trinidad And Tobago</option>
         <option value="TN">Tunisia</option>
         <option value="TR">Turkey</option>
         <option value="TM">Turkmenistan</option>
         <option value="TC">Turks And Caicos Islands</option>
         <option value="TV">Tuvalu</option>
         <option value="UG">Uganda</option>
         <option value="UA">Ukraine</option>
         <option value="AE">United Arab Emirates</option>
         <option value="GB">United Kingdom</option>
         <option value="US">United States</option>
         <option value="UM">United States Minor Outlying Islands</option>
         <option value="UY">Uruguay</option>
         <option value="UZ">Uzbekistan</option>
         <option value="VU">Vanuatu</option>
         <option value="VE">Venezuela</option>
         <option value="VN">Viet Nam</option>
         <option value="VG">Virgin Islands (British)</option>
         <option value="VI">Virgin Islands (U.S.)</option>
         <option value="WF">Wallis And Futuna Islands</option>
         <option value="EH">Western Sahara</option>
         <option value="YE">Yemen</option>
         <option value="YU">Yugoslavia</option>
         <option value="ZM">Zambia</option>
         <option value="ZW">Zimbabwe</option>
      </select>
   </div>
   <div class="form-group"><label for="name">State *</label>
      <input id="state" class="form-control" required="" type="text" placeholder="" name="interdistiform_state" />
   </div>
   <div class="form-group"><label for="addqty">Phone *</label>
      <input id="phone" name="interdistiform_mobile" class="form-control" required="" type="number" placeholder="" />
   </div>
   <div class="form-group"><label for="email">Email *</label>
      <input id="email" class="form-control" name="interdistiform_email" required="" type="email" placeholder="" />
   </div>
   <div class="form-group"><label for="address">Address *</label>
      <input id="address" class="form-control" required="" type="text" name="interdistiform_address" placeholder="" />
   </div>
   <div class="form-group">
      <label for="englishyesno">Are You Comfortable With English? *</label>
      <select id="compfortable_with_english" class="" name="englishyesno" name="interdistiform_compfortable_with_english" required="" aria-invalid="false" aria-describedby="">
         <option selected="selected" value="1">Yes</option>
         <option value="0">No</option>
      </select>
   </div>
   <div class="form-group"><label for="comment">Comment</label>
      <textarea id="comment" name="interdistiform_comment" class="resize-vertical" name="" aria-invalid="false" aria-describedby=""></textarea>
   </div>
   <input type="hidden" name="ga_source" id="ga_source" value="<?php echo $_GET['utm_source'];?>">
   <input type="hidden" name="ga_medium" id="ga_medium" value="<?php echo $_GET['utm_medium'];?>">
   <input type="hidden" name="ga_campaign" id="ga_campaign" value="<?php echo $_GET['utm_campaign'];?>">
   <input type="submit" value="Submit" name="interdist" />
</form>
<span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span>
	</div>

<?php else : ?>
<div id="primary" class="main-content-inner-full">
<?php endif; ?> 
    <div id="content" class="site-content" role="main">
      <?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
					// Include the page content template.
					get_template_part( 'content', 'page' ); ?>
      <?php endwhile;
			?>
    </div><!-- #content -->
</div><!-- #primary -->
   <?php 
if (get_option('tmpmela_page_sidebar') == 'yes') : 
	get_sidebar( 'content' );
	get_sidebar();
endif;  ?><!-- #main-content -->
</div>
<script type="text/javascript">
	function DistributorshipInternational(){
		var type = 1;
		var name = jQuery('#name').val();
		var phone = jQuery('#phone').val();
		var country = jQuery('#country').val();
		var state = jQuery('#state').val();
		var email = jQuery('#email').val();
		var address = jQuery('#address').val();
		var compfortable_with_english = jQuery('#compfortable_with_english').val();
		var comment = jQuery('#comment').val();
		var ga_campaign = jQuery('#ga_campaign').val();
		var ga_medium = jQuery('#ga_medium').val();
		var ga_campaign = jQuery('#ga_campaign').val();
	jQuery.ajax({
		url : "http://panel.orpatgroup.com/api/internationalDistributorship",
		method: 'POST',
		data : {
			 "name": name,
		 "mobile": phone,
		 "country": country,
		 "state": state,
		 "email": email,
		 "address": address,
		 "compfortable_with_english": compfortable_with_english,
		 "comment": comment,
		 "ga_campaign" : ga_campaign ,
		 "ga_medium" : ga_medium,
		 "ga_campaign" :ga_campaign
		},
		success : function(data){
			console.log(data);
		}
	});
}
</script>
<?php get_footer(); ?>