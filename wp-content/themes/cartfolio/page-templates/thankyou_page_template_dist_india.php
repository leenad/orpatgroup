<?php
/* Template Name: thankyou page template distributorship in india*/ 
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11"/>
<link rel="pingback" href="https://orpatgroup.com/xmlrpc.php"/>
 				<script>document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
			<title>corporate gifting thankyou page - Orpat Group</title>
<!--[if lt IE 9]>
		<![endif]-->

<!-- This site is optimized with the Yoast SEO plugin v12.1 - https://yoast.com/wordpress/plugins/seo/ -->
<!-- Admin only notice: this page does not show a meta description because it does not have one, either write it for this page specifically or go into the [SEO - Search Appearance] menu and set up a template. -->
<link rel="canonical" href="https://orpatgroup.com/distributorship-in-india-thankyou-page/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="corporate gifting thankyou page - Orpat Group" />
<meta property="og:url" content="https://orpatgroup.com/distributorship-in-india-thankyou-page/" />
<meta property="og:site_name" content="Orpat Group" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="corporate gifting thankyou page - Orpat Group" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://orpatgroup.com/#website","url":"https://orpatgroup.com/","name":"Orpat Group","potentialAction":{"@type":"SearchAction","target":"https://orpatgroup.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://orpatgroup.com/distributorship-in-india-thankyou-page/#webpage","url":"https://orpatgroup.com/distributorship-in-india-thankyou-page/","inLanguage":"en-US","name":"corporate gifting thankyou page - Orpat Group","isPartOf":{"@id":"https://orpatgroup.com/#website"},"datePublished":"2019-10-31T09:58:04+00:00","dateModified":"2019-10-31T10:13:06+00:00"}]}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Orpat Group &raquo; Feed" href="https://orpatgroup.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Orpat Group &raquo; Comments Feed" href="https://orpatgroup.com/comments/feed/" />
		<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/orpatgroup.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.12"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='dashicons-css'  href='https://orpatgroup.com/wp-includes/css/dashicons.min.css?ver=4.9.12' media='all' />
<style id='dashicons-inline-css'>
[data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
</style>
<link rel='stylesheet' id='admin-bar-css'  href='https://orpatgroup.com/wp-includes/css/admin-bar.min.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='google-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C600%2C800%7CJosefin%2BSans%3A300%2C400%2C600%2C600&#038;ver=1.0.0#038;subset=latin%2Clatin-ext' media='all' />
<link rel='stylesheet' id='tmpmela-block-style-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/blocks.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='tmpmela-isotope-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/isotop-port.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='tmpmela-custom-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/custom.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='owl-carousel-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/owl.carousel.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='owl-transitions-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/owl.transitions.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='shadowbox-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/shadowbox.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='tmpmela-shortcode-style-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/shortcode_style.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='animate-min-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/animate.min.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='tmpmela-woocommerce-css-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/css/megnor/woocommerce.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://orpatgroup.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://orpatgroup.com/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8.3' media='all' />
<style id='rs-plugin-settings-inline-css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='apm-styles-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce-accepted-payment-methods/assets/css/style.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='woof-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/css/front.css?ver=1.2.3' media='all' />
<link rel='stylesheet' id='chosen-drop-down-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/chosen/chosen.min.css?ver=1.2.3' media='all' />
<link rel='stylesheet' id='plainoverlay-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/css/plainoverlay.css?ver=1.2.3' media='all' />
<style id='woocommerce-inline-inline-css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='font-awesome-css'  href='https://orpatgroup.com/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=6.0.3' media='all' />
<style id='font-awesome-inline-css'>
[data-font="FontAwesome"]:before {font-family: 'FontAwesome' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
</style>
<link rel='stylesheet' id='flaticon-css'  href='https://orpatgroup.com/wp-content/plugins/wphobby-pdf-invoices-for-woocommerce/assets/css/flaticon.css?ver=1.0.2' media='all' />
<link rel='stylesheet' id='whpdf-frontend-style-css'  href='https://orpatgroup.com/wp-content/plugins/wphobby-pdf-invoices-for-woocommerce/assets/css/frontend.css?ver=1.0.2' media='all' />
<link rel='stylesheet' id='jquery-colorbox-css'  href='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-compare/assets/css/colorbox.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='yith-quick-view-css'  href='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.9.12' media='all' />
<style id='yith-quick-view-inline-css'>

				#yith-quick-view-modal .yith-wcqv-main{background:#ffffff;}
				#yith-quick-view-close{color:#cdcdcd;}
				#yith-quick-view-close:hover{color:#ff0000;}
</style>
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='//orpatgroup.com/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.9.12' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=1.2.0' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.7.0' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=2.2.13' media='all' />
<style id='yith-wcwl-main-inline-css'>
.wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }
</style>
<link rel='stylesheet' id='tmpmela-fonts-css'  href='//fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C600%2C300italic%2C400italic%2C600italic%7CBitter%3A400%2C600&#038;subset=latin%2Clatin-ext' media='all' />
<link rel='stylesheet' id='FontAwesome-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/fonts/css/font-awesome.css?ver=4.7.0' media='all' />
<link rel='stylesheet' id='tmpmela-style-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/style.css?ver=1.0' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='vc_lte_ie9-css'  href='https://orpatgroup.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?ver=6.0.3' media='screen' />
<![endif]-->
<link rel='stylesheet' id='yoast-seo-adminbar-css'  href='https://orpatgroup.com/wp-content/plugins/wordpress-seo/css/dist/adminbar-1210.min.css?ver=12.1' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='https://orpatgroup.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.3' media='all' />
<style id='js_composer_front-inline-css'>
#catalog .widget.widget_text  {
    padding: 20px;
    margin: 0px 10px 10px 0px;
    background: #eaeaea;
}
.vc_custom_1570626165418{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1570626165418{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}
</style>
<script src='https://orpatgroup.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script src='https://orpatgroup.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script src='https://orpatgroup.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/orpatgroup.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.7.0'></script>
<script src='https://orpatgroup.com/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=6.0.3'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.jqtransform.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.jqtransform.script.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.custom.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/megnor.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/carousel.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.easypiechart.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/custom.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/owl.carousel.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.formalize.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/respond.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.validate.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/shadowbox.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/waypoints.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.megamenu.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/easyResponsiveTabs.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.treeview.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/jquery.jscroll.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/countUp.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/doubletaptogo.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/html5.js?ver=4.9.12'></script>
<script>
/* <![CDATA[ */
var php_var = {"tmpmela_loadmore":"","tmpmela_pagination":"","tmpmela_nomore":""};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/megnor/megnorloadmore.js?ver=4.9.12'></script>
<link rel='https://api.w.org/' href='https://orpatgroup.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://orpatgroup.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://orpatgroup.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.12" />
<meta name="generator" content="WooCommerce 3.7.0" />
<link rel='shortlink' href='https://orpatgroup.com/?p=13558' />
<link rel="alternate" type="application/json+oembed" href="https://orpatgroup.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Forpatgroup.com%2Fcorporate-gifting-thankyou-page%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://orpatgroup.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Forpatgroup.com%2Fcorporate-gifting-thankyou-page%2F&#038;format=xml" />
<meta name="generator" content="/var/www/html/orpatgroup/wp-content/themes/cartfolio/style.css - " /><link rel="shortcut icon" type="image/png" href="https://orpatgroup.com/wp-content/themes/cartfolio/templatemela/favicon.ico" />		<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet' />
	<style>
		h1 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
			h1 {	
		color:#000000;	
	}	
			h2 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
			h2 {	
		color:#000000;	
	}	
			h3 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
			h3 { color:#000000;}
			h4 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
		
		h4 {	
		color:#000000;	
	}	
			h5 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
			h5 {	
		color:#000000;	
	}	
			h6 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
		
		h6 {	
		color:#000000;	
	}	
			.home-service h3.widget-title {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;
	}	
		a {
		color:#000000;
	}
	a:hover ,
	li.product a:hover .product-name, .entry-meta a:hover , .tabs a.current, a.active, .entry-thumbnail .comments-link a:hover,
	.cat-outer-block .cat_description a:hover ,.post-detail a:hover ,.current-cat a{
		color:#E7040F;
	}
	.site-footer .widget-title{
		color:#000000; 
	}
	.footer a, .site-footer a, .site-footer{
		color:#838383; 
	}
	.footer a:hover, .footer .footer-links li a:hover, .site-footer a:hover {
		color:#E7040F;		 
	}
	.site-footer
	{
		background-color:#FFFFFF;
			}
		h3 {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;	
	}	
		
		.site-footer {	
		font-family:'Poppins', Arial, Helvetica, sans-serif;	
	}	
		
	.site-footer {
		background-color:rgb(255,255,255); 
	}	
	body {
		background-color:#FFFFFF ;
				background-image: url("https://orpatgroup.com/wp-content/themes/cartfolio/images/megnor/colorpicker/pattern/body-bg.png");
		background-position:top left ;
		background-repeat:repeat;
		background-attachment:scroll;
					
		color:#838383;
	} 	
	.mega-menu ul li a{color:#FFFFFF; }
	.mega-menu ul li a:hover,.mega-menu .current_page_item > a{color:#6AE751; }	

	.mega-menu ul li .sub a{color:#000000; }
	.mega-menu ul li .sub a:hover{color:#E7040F; }
	
	.mega-menu ul li .sub {background-color:#FFFFFF ;	}

	.home .site-header {
		background-color:rgba(0,0,0,0.0);
		}
	.home .site-header:hover,.sticky-menu .header-style, .home.blog .site-header,
	.site-header{
		background-color:rgba(0,0,0,1);
			} 
		body {	
		font-family: 'Poppins', Arial, Helvetica, sans-serif;	
	}
.widget button, .widget input[type="button"], .widget input[type="reset"], .widget input[type="submit"], a.button, button, .contributor-posts-link, input[type="button"], input[type="reset"], input[type="submit"], .button_content_inner a, .woocommerce #content input.button, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce-page #content input.button, .woocommerce-page #respond input#submit, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button, .woocommerce .wishlist_table td.product-add-to-cart a,.woocommerce .wc-proceed-to-checkout .checkout-button:hover,
.woocommerce-page input.button:hover,.woocommerce #content input.button.disabled,.woocommerce #content input.button:disabled,.woocommerce #respond input#submit.disabled,.woocommerce #respond input#submit:disabled,.woocommerce a.button.disabled,.woocommerce a.button:disabled,.woocommerce button.button.disabled,.woocommerce button.button:disabled,.woocommerce input.button.disabled,.woocommerce input.button:disabled,.woocommerce-page #content input.button.disabled,.woocommerce-page #content input.button:disabled,.woocommerce-page #respond input#submit.disabled,.woocommerce-page #respond input#submit:disabled,.woocommerce-page a.button.disabled,.woocommerce-page a.button:disabled,.woocommerce-page button.button.disabled,.woocommerce-page button.button:disabled,.woocommerce-page input.button.disabled,.woocommerce-page input.button:disabled, .loadgridlist-wrapper .woocount{
	background-color:rgba(231,4,15,1);
	color:#FFFFFF;
			font-family:'Poppins', Arial, Helvetica, sans-serif;
	}
.widget input[type="button"]:hover,.widget input[type="button"]:focus,.widget input[type="reset"]:hover,.widget input[type="reset"]:focus,.widget input[type="submit"]:hover,.widget input[type="submit"]:focus,a.button:hover,a.button:focus,button:hover,button:focus,.contributor-posts-link:hover,input[type="button"]:hover,input[type="button"]:focus,input[type="reset"]:hover,input[type="reset"]:focus,input[type="submit"]:hover,input[type="submit"]:focus,.calloutarea_button a.button:hover,.calloutarea_button a.button:focus,.button_content_inner a:hover,.button_content_inner a:focus,.woocommerce #content input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-page #content input.button:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page button.button:hover, .woocommerce #content table.cart .checkout-button:hover,#primary .entry-summary .single_add_to_cart_button:hover,.woocommerce .wc-proceed-to-checkout .checkout-button, .loadgridlist-wrapper .woocount:hover{
	background-color:rgba(0,0,0,1);
	color:#FFFFFF;
	}	
			
</style>
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<style media="print">#wpadminbar { display:none; }</style>
<style media="screen">
	html { margin-top: 0px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<meta name="generator" content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<style></style><link rel="icon" href="https://orpatgroup.com/wp-content/uploads/2019/09/favicon.png" sizes="32x32" />
<link rel="icon" href="https://orpatgroup.com/wp-content/uploads/2019/09/favicon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://orpatgroup.com/wp-content/uploads/2019/09/favicon.png" />
<meta name="msapplication-TileImage" content="https://orpatgroup.com/wp-content/uploads/2019/09/favicon.png" />
<script>function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
		<style id="wp-custom-css">
			

/* .header-main.site-header-fix.header-style {
    background-color: rgba(0,0,0,1) !important;
    position: fixed;
    top: 0;
    width: 100%;
	left:0;
} */
.site-main {
    margin: 0;
    padding-top: 95px;
}
.tp-rightarrow.tparrows.custom, .tp-leftarrow.tparrows.custom{
	top:95% !important;
}
header#masthead {
    background: #000000;
    position: fixed;
	width: 100%;
}

.home .site-header {
/*     background-color: #ffffff; */
}
.header-logo img {
    width: 63%;
}
.display-none{
	display:none;
}
input.tnp-firstname {
    display: none;
}
aside#leftbannerwidget-1 {
    display: none;
}
.cat_description {
    display: none !important;
}
#orpat-eliectricals .icon {
    display: none;
}
#orpat-trust .service-content.style-2,  .pointer-events-none {
    pointer-events: none !important;
}
.no-fa-icon .icon {
    display: none !important;
}
.mobile-slider{
	display:none;
}
#shipping_company_field, #billing_company_field, #menu-item-8230, .woof.woof_sid.woof_sid_auto_shortcode {
    display: none;
}
form.woocommerce-ordering {
    color: #000000;
    border: 1px solid #000000;
}
.customSelectInner {
    width: 180px !important;
}
a.compare.button, .star-rating, aside#woocommerce_product_tag_cloud-1 , .woof_products_top_panel, .woocommerce .woocommerce-info, .woocommerce-page .woocommerce-info{
    display: none !important;
}
ul.woocommerce-error li {
    color: red;
}
p.woocommerce-notice.woocommerce-notice--success.woocommerce-thankyou-order-received {
    text-align: center;
    font-size: 36px;
    color: green;
}
.header-center {
    width: 1000px;
}
.sub-container.mega {
    overflow-x: scroll;
    height: 500px;
}
.sub-container.mega::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

.sub-container.mega::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}
.sub-container.mega::-webkit-scrollbar-thumb
{
	background-color: #000000;
}
.woocommerce form.checkout_coupon, .woocommerce-page form.checkout_coupon {
	display: block !important;
}


.woocommerce #payment #place_order, .woocommerce-page #payment #place_order {
    font-size: 20px;
    text-align: center;
    margin: 0 auto;
    display: block;
    float: none;
    width: 27%;
}
li.woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--dashboard, li.woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--downloads {
    display: none;
}
.footer-menu-links ul {
	width: 72%;
	margin:0 auto;
}
.formpage .form-outer{
		width: 60%;
		margin:0 auto;
	}
.formpage form{
		width: 55%;
		margin:0 auto;
	}
.formpage .text{
		text-align: center;
	}

@media(max-width:1440px){
	.header-center {
    width: 63%;
	}
	.formpage .form-outer{
		width: 70%;
	}
.formpage form{
		width: 65%;
	}
}

@media(max-width:1366px){
	.footer-menu-links ul {
    width: 92%;
	}
}

@media (max-width: 1366px){
	.footer-menu-links ul {
			width: 100%;
	}
		.formpage .form-outer{
			width: 80%;
		}
	.formpage form{
			width: 75%;
		}
}

@media(max-width:768px){
	.comment-content img[height], .entry-content img, .entry-summary img, img[class*="align"], img[class*="wp-image-"], img[class*="attachment-"], #site-header img{
/* 		width: 100% !important;
    height: auto !important;
    display: block; */
	}
	.desktop-slider{
	display:none;
}
	.mobile-slider{
	display:block;
}
	img.image.wp-image-11794.attachment-full.size-full{
		width:180px;
	}
	.woocommerce #payment #place_order, .woocommerce-page #payment #place_order {
    width: 45%;
}
	p.woocommerce-notice.woocommerce-notice--success.woocommerce-thankyou-order-received {
    font-size: 32px;
	}
}

@media(max-width:767px){
		.formpage .form-outer{
			width: 100%;
		}
	.formpage form{
			width: 90%;
		}
}


@media(max-width:600px){
		.woocommerce #payment #place_order, .woocommerce-page #payment #place_order {
    width: 100%;
	}
	.woocommerce form.checkout_coupon, .woocommerce-page form.checkout_coupon {
    width: 88%;
}
	p.woocommerce-notice.woocommerce-notice--success.woocommerce-thankyou-order-received{
		line-height:1.2;
	}
}

@media(max-width:375px){
		.woocommerce form.checkout_coupon, .woocommerce-page form.checkout_coupon {
    width: 87%;
}
	p.woocommerce-notice.woocommerce-notice--success.woocommerce-thankyou-order-received {
    font-size: 23px;
}
}

@media(max-width:320px){
		.woocommerce form.checkout_coupon, .woocommerce-page form.checkout_coupon {
    width: 85%;
}
}


		</style>
	        <script>
            var woof_is_permalink =1;

            var woof_shop_page = "";
        
            var woof_really_curr_tax = {};
            var woof_current_page_link = location.protocol + '//' + location.host + location.pathname;
            //***lets remove pagination from woof_current_page_link
            woof_current_page_link = woof_current_page_link.replace(/\page\/[0-9]+/, "");
                            woof_current_page_link = "https://orpatgroup.com/shop/";
                            var woof_link = 'https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/';

                </script>

                <script>

            var woof_ajaxurl = "https://orpatgroup.com/wp-admin/admin-ajax.php";

            var woof_lang = {
                'orderby': "orderby",
                'date': "date",
                'perpage': "per page",
                'pricerange': "price range",
                'menu_order': "menu order",
                'popularity': "popularity",
                'rating': "rating",
                'price': "price low to high",
                'price-desc': "price high to low"
            };

            if (typeof woof_lang_custom == 'undefined') {
                var woof_lang_custom = {};/*!!important*/
            }

            //***

            var woof_is_mobile = 0;
        


            var woof_show_price_search_button = 0;
            var woof_show_price_search_type = 0;
        
            var woof_show_price_search_type = 2;

            var swoof_search_slug = "swoof";

        
            var icheck_skin = {};
                        icheck_skin = 'none';
        
            var is_woof_use_chosen =1;

        

            var woof_current_values = '[]';
            //+++
            var woof_lang_loading = "Loading ...";

        
            var woof_lang_show_products_filter = "show products filter";
            var woof_lang_hide_products_filter = "hide products filter";
            var woof_lang_pricerange = "price range";

            //+++

            var woof_use_beauty_scroll =0;
            //+++
            var woof_autosubmit =1;
            var woof_ajaxurl = "https://orpatgroup.com/wp-admin/admin-ajax.php";
            /*var woof_submit_link = "";*/
            var woof_is_ajax = 0;
            var woof_ajax_redraw = 0;
            var woof_ajax_page_num =1;
            var woof_ajax_first_done = false;
            var woof_checkboxes_slide_flag = true;


            //toggles
            var woof_toggle_type = "text";

            var woof_toggle_closed_text = "-";
            var woof_toggle_opened_text = "+";

            var woof_toggle_closed_image = "https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/img/plus3.png";
            var woof_toggle_opened_image = "https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/img/minus3.png";


            //indexes which can be displayed in red buttons panel
                    var woof_accept_array = ["min_price", "orderby", "perpage", ,"product_visibility","product_cat","product_tag","pa_color","pa_size"];

        


            //***
            //for extensions

            var woof_ext_init_functions = null;
        

        
            var woof_overlay_skin = "plainoverlay";

            jQuery(function () {
                try
                {
                    woof_current_values = jQuery.parseJSON(woof_current_values);
                } catch (e)
                {
                    woof_current_values = null;
                }
                if (woof_current_values == null || woof_current_values.length == 0) {
                    woof_current_values = {};
                }

            });

            function woof_js_after_ajax_done() {
                jQuery(document).trigger('woof_ajax_done');
                    }
        </script>
        <noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript> 


 <!-- Global site tag (gtag.js) - Google Ads: 720080772 -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-720080772"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-720080772');
</script> -->
<!-- Event snippet for Orpat Group conversion page -->
<!-- <script>
  gtag('event', 'conversion', {'send_to': 'AW-720080772/qOWzCIu8iacBEISfrtcC'});
</script> -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M4NCQ53');</script>
<!-- End Google Tag Manager -->


</head>

<!-- event code-->
<script>
  fbq('track', 'Lead');
</script>


<body class="page-template page-template-page-templates page-template-home page-template-page-templateshome-php page page-id-13558 logged-in admin-bar no-customize-support woocommerce-no-js masthead-fixed singular left-sidebar wpb-js-composer js-comp-ver-6.0.3 vc_responsive">
<div id="page" class="hfeed site">
<!-- Header -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4NCQ53"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header id="masthead" class="site-header header-fix header full-width">
	<div class="header-main site-header-fix">
										<!-- Start header_left -->	
				<div class="header-left">	
						<!-- Header LOGO-->
							<div class="header-logo">
															<a href="https://orpatgroup.com/" title="Orpat Group" rel="home">
								<img alt="" src="https://orpatgroup.com/wp-content/uploads/2019/09/orpat-logo-white.png" />								</a>
																					</div>
							<!-- Header Mob LOGO-->
							<div class="header-mob-logo">
															<a href="https://orpatgroup.com/" title="Orpat Group" rel="home">
								<img alt="" src="https://orpatgroup.com/wp-content/uploads/2019/09/orpat-logo-white.png" />								</a>
																					</div>					 						
				</div>				
				<!-- Start header_center -->	
				<div class="header-center">
					<!-- #site-navigation -->
					<nav id="site-navigation" class="navigation-bar main-navigation">																				
					<a class="screen-reader-text skip-link" href="#content" title="Skip to content">Skip to content</a>	
						<div class="mega-menu">
							<div class="menu-mainmenu-container"><ul id="menu-mainmenu" class="mega"><li id="menu-item-7179" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7179"><a href="https://orpatgroup.com/">Home</a></li>
<li id="menu-item-11095" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11095"><a href="https://orpatgroup.com/about-us/">About Us</a></li>
<li id="menu-item-13006" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13006"><a href="#">Products</a>
<ul class="sub-menu">
	<li id="menu-item-11091" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11091"><a href="https://orpatgroup.com/product-category/ajanta-clocks/">Ajanta Clocks</a>
	<ul class="sub-menu">
		<li id="menu-item-13007" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13007"><a href="https://orpatgroup.com/product-category/ajanta-clocks/designer-sweep-second-clocks/">Designer Sweep Second Clocks</a></li>
		<li id="menu-item-13008" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13008"><a href="https://orpatgroup.com/product-category/ajanta-clocks/digital-clocks/">Digital Clocks</a></li>
		<li id="menu-item-13009" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13009"><a href="https://orpatgroup.com/product-category/ajanta-clocks/fancy-clocks/">Fancy Clocks</a></li>
		<li id="menu-item-13010" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13010"><a href="https://orpatgroup.com/product-category/ajanta-clocks/fancy-pendulum-clocks/">Fancy Pendulum Clocks</a></li>
		<li id="menu-item-13011" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13011"><a href="https://orpatgroup.com/product-category/ajanta-clocks/grandfather-clocks/">Grandfather Clocks</a></li>
		<li id="menu-item-13012" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13012"><a href="https://orpatgroup.com/product-category/ajanta-clocks/magic-moments-clocks/">Magic Moments Clocks</a></li>
		<li id="menu-item-13013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13013"><a href="https://orpatgroup.com/product-category/ajanta-clocks/musical-pendulum-clocks/">Musical Pendulum Clocks</a></li>
		<li id="menu-item-13014" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13014"><a href="https://orpatgroup.com/product-category/ajanta-clocks/plain-musical-clocks/">Plain Musical Clocks</a></li>
		<li id="menu-item-13015" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13015"><a href="https://orpatgroup.com/product-category/ajanta-clocks/simple-clocks/">Simple Clocks</a></li>
		<li id="menu-item-13016" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13016"><a href="https://orpatgroup.com/product-category/ajanta-clocks/solid-wood-cuckoo-clocks/">Solid Wood Cuckoo Clocks</a></li>
		<li id="menu-item-13017" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13017"><a href="https://orpatgroup.com/product-category/ajanta-clocks/talking-clocks-clocks/">Talking Clocks</a></li>
		<li id="menu-item-13018" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13018"><a href="https://orpatgroup.com/product-category/ajanta-clocks/wooden-pendulum-glass-clocks/">Wooden Pendulum &#038; Glass Clocks</a></li>
		<li id="menu-item-13019" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13019"><a href="https://orpatgroup.com/product-category/ajanta-clocks/wooden-simple-clocks/">Wooden Simple Clocks</a></li>
		<li id="menu-item-13020" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13020"><a href="https://orpatgroup.com/product-category/ajanta-clocks/wooden-sweep-second-clocks/">Wooden Sweep Second Clocks</a></li>
	</ul>
</li>
	<li id="menu-item-13021" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13021"><a href="https://orpatgroup.com/product-category/home-appliances/">Home Appliances</a>
	<ul class="sub-menu">
		<li id="menu-item-13022" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13022"><a href="https://orpatgroup.com/product-category/home-appliances/steam-irons/">Iron</a></li>
		<li id="menu-item-13023" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13023"><a href="https://orpatgroup.com/product-category/home-appliances/kettles/">Kettles</a></li>
		<li id="menu-item-13024" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13024"><a href="https://orpatgroup.com/product-category/home-appliances/choppers/">Choppers</a></li>
		<li id="menu-item-13025" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13025"><a href="https://orpatgroup.com/product-category/home-appliances/hand-blenders/">Hand Blenders</a></li>
		<li id="menu-item-13026" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13026"><a href="https://orpatgroup.com/product-category/home-appliances/oil-heaters/">Heaters</a></li>
	</ul>
</li>
	<li id="menu-item-13027" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13027"><a href="https://orpatgroup.com/product-category/orpat-timepieces/">Timepieces</a>
	<ul class="sub-menu">
		<li id="menu-item-13028" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13028"><a href="https://orpatgroup.com/product-category/orpat-timepieces/bell-alarm-table-clocks/">Bell Alarm Table Clocks</a></li>
		<li id="menu-item-13029" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13029"><a href="https://orpatgroup.com/product-category/orpat-timepieces/digital-alarm-table-clocks/">Digital Alarm Table Clocks</a></li>
		<li id="menu-item-13031" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13031"><a href="https://orpatgroup.com/product-category/orpat-timepieces/musical-alarm-table-clocks/">Musical Alarm Table Clocks</a></li>
		<li id="menu-item-13032" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13032"><a href="https://orpatgroup.com/product-category/orpat-timepieces/simple-buzzer-alarm-table-clocks/">Simple Buzzer Alarm Table Clocks</a></li>
		<li id="menu-item-13033" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13033"><a href="https://orpatgroup.com/product-category/orpat-timepieces/snooze-alarm-table-clocks/">Snooze Alarm Table Clocks</a></li>
	</ul>
</li>
	<li id="menu-item-13034" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13034"><a href="https://orpatgroup.com/product-category/orpat-calculators/">Orpat Calculators</a>
	<ul class="sub-menu">
		<li id="menu-item-13035" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13035"><a href="https://orpatgroup.com/product-category/orpat-calculators/basic-calculators/">Basic Calculators</a></li>
		<li id="menu-item-13036" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13036"><a href="https://orpatgroup.com/product-category/orpat-calculators/check-correct-calculators/">Check &#038; Correct Calculators</a></li>
		<li id="menu-item-13037" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13037"><a href="https://orpatgroup.com/product-category/orpat-calculators/scientific-calculators/">Scientific Calculators</a></li>
	</ul>
</li>
	<li id="menu-item-13038" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13038"><a href="https://orpatgroup.com/product-category/orpat-fans/">Orpat Fans</a>
	<ul class="sub-menu">
		<li id="menu-item-13039" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13039"><a href="https://orpatgroup.com/product-category/orpat-fans/economy-fans/">Economy Fans</a></li>
		<li id="menu-item-13040" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13040"><a href="https://orpatgroup.com/product-category/orpat-fans/exhaust-fans/">Exhaust Fans</a></li>
		<li id="menu-item-13041" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13041"><a href="https://orpatgroup.com/product-category/orpat-fans/pedestal-fans/">Pedestal Fans</a></li>
		<li id="menu-item-13042" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13042"><a href="https://orpatgroup.com/product-category/orpat-fans/premium-fans/">Premium Fans</a></li>
		<li id="menu-item-13043" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13043"><a href="https://orpatgroup.com/product-category/orpat-fans/table-fans/">Table Fans</a></li>
		<li id="menu-item-13044" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13044"><a href="https://orpatgroup.com/product-category/orpat-fans/ventilation-fans/">Ventilation Fans</a></li>
		<li id="menu-item-13045" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13045"><a href="https://orpatgroup.com/product-category/orpat-fans/wall-fans/">Wall Fans</a></li>
	</ul>
</li>
	<li id="menu-item-11092" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11092"><a href="https://orpatgroup.com/product-category/orpat-switches/">Orpat Switches</a>
	<ul class="sub-menu">
		<li id="menu-item-13046" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13046"><a href="https://orpatgroup.com/product-category/orpat-switches/platinum-series-fan-regulators/">Platinum Series Fan Regulators</a></li>
		<li id="menu-item-13047" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13047"><a href="https://orpatgroup.com/product-category/orpat-switches/platinum-series-sockets/">Platinum Series Sockets</a></li>
		<li id="menu-item-13048" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13048"><a href="https://orpatgroup.com/product-category/orpat-switches/platinum-series-switches/">Platinum Series Switches</a></li>
		<li id="menu-item-13049" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13049"><a href="https://orpatgroup.com/product-category/orpat-switches/silver-series-cases-covers/">Silver Series Cases &#038; Covers</a></li>
	</ul>
</li>
	<li id="menu-item-13050" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13050"><a href="https://orpatgroup.com/product-category/orpat-telephones/">Orpat Telephones</a>
	<ul class="sub-menu">
		<li id="menu-item-13051" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13051"><a href="https://orpatgroup.com/product-category/orpat-telephones/basic-phones/">Basic Phones</a></li>
		<li id="menu-item-13052" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13052"><a href="https://orpatgroup.com/product-category/orpat-telephones/caller-id-phones/">Caller ID Phones</a></li>
	</ul>
</li>
	<li id="menu-item-7176" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7176"><a href="https://orpatgroup.com/product-category/orpat-toys/">Orpat Toys</a>
	<ul class="sub-menu">
		<li id="menu-item-7178" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7178"><a href="https://orpatgroup.com/product-category/orpat-toys/educational_toys/">Educational Toys</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-13059" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13059"><a href="https://orpatgroup.com/corporate-gifting/">Corporate Gifting</a></li>
<li id="menu-item-13056" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13056"><a href="#">Partner With Us</a>
<ul class="sub-menu">
	<li id="menu-item-13057" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13057"><a href="https://orpatgroup.com/distributorship-in-india/">Distributorship in India</a></li>
	<li id="menu-item-13058" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13058"><a href="https://orpatgroup.com/international-distributorship/">International Distributorship</a></li>
</ul>
</li>
</ul></div>		
						</div>	
					</nav>
				</div>	
				<!-- Start header_right -->	
				<div class="header-right">
				<nav class="mobile-navigation">	
				<h3 class="menu-toggle">Menu</h3>
					<div class="mobile-menu">	
						<span class="close-menu"></span>	
							<div class="menu-mainmenu-container"><ul id="menu-mainmenu-1" class="mobile-menu-inner"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7179"><a href="https://orpatgroup.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11095"><a href="https://orpatgroup.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13006"><a href="#">Products</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11091"><a href="https://orpatgroup.com/product-category/ajanta-clocks/">Ajanta Clocks</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13007"><a href="https://orpatgroup.com/product-category/ajanta-clocks/designer-sweep-second-clocks/">Designer Sweep Second Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13008"><a href="https://orpatgroup.com/product-category/ajanta-clocks/digital-clocks/">Digital Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13009"><a href="https://orpatgroup.com/product-category/ajanta-clocks/fancy-clocks/">Fancy Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13010"><a href="https://orpatgroup.com/product-category/ajanta-clocks/fancy-pendulum-clocks/">Fancy Pendulum Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13011"><a href="https://orpatgroup.com/product-category/ajanta-clocks/grandfather-clocks/">Grandfather Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13012"><a href="https://orpatgroup.com/product-category/ajanta-clocks/magic-moments-clocks/">Magic Moments Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13013"><a href="https://orpatgroup.com/product-category/ajanta-clocks/musical-pendulum-clocks/">Musical Pendulum Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13014"><a href="https://orpatgroup.com/product-category/ajanta-clocks/plain-musical-clocks/">Plain Musical Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13015"><a href="https://orpatgroup.com/product-category/ajanta-clocks/simple-clocks/">Simple Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13016"><a href="https://orpatgroup.com/product-category/ajanta-clocks/solid-wood-cuckoo-clocks/">Solid Wood Cuckoo Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13017"><a href="https://orpatgroup.com/product-category/ajanta-clocks/talking-clocks-clocks/">Talking Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13018"><a href="https://orpatgroup.com/product-category/ajanta-clocks/wooden-pendulum-glass-clocks/">Wooden Pendulum &#038; Glass Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13019"><a href="https://orpatgroup.com/product-category/ajanta-clocks/wooden-simple-clocks/">Wooden Simple Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13020"><a href="https://orpatgroup.com/product-category/ajanta-clocks/wooden-sweep-second-clocks/">Wooden Sweep Second Clocks</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13021"><a href="https://orpatgroup.com/product-category/home-appliances/">Home Appliances</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13022"><a href="https://orpatgroup.com/product-category/home-appliances/steam-irons/">Iron</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13023"><a href="https://orpatgroup.com/product-category/home-appliances/kettles/">Kettles</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13024"><a href="https://orpatgroup.com/product-category/home-appliances/choppers/">Choppers</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13025"><a href="https://orpatgroup.com/product-category/home-appliances/hand-blenders/">Hand Blenders</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13026"><a href="https://orpatgroup.com/product-category/home-appliances/oil-heaters/">Heaters</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13027"><a href="https://orpatgroup.com/product-category/orpat-timepieces/">Timepieces</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13028"><a href="https://orpatgroup.com/product-category/orpat-timepieces/bell-alarm-table-clocks/">Bell Alarm Table Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13029"><a href="https://orpatgroup.com/product-category/orpat-timepieces/digital-alarm-table-clocks/">Digital Alarm Table Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13031"><a href="https://orpatgroup.com/product-category/orpat-timepieces/musical-alarm-table-clocks/">Musical Alarm Table Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13032"><a href="https://orpatgroup.com/product-category/orpat-timepieces/simple-buzzer-alarm-table-clocks/">Simple Buzzer Alarm Table Clocks</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13033"><a href="https://orpatgroup.com/product-category/orpat-timepieces/snooze-alarm-table-clocks/">Snooze Alarm Table Clocks</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13034"><a href="https://orpatgroup.com/product-category/orpat-calculators/">Orpat Calculators</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13035"><a href="https://orpatgroup.com/product-category/orpat-calculators/basic-calculators/">Basic Calculators</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13036"><a href="https://orpatgroup.com/product-category/orpat-calculators/check-correct-calculators/">Check &#038; Correct Calculators</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13037"><a href="https://orpatgroup.com/product-category/orpat-calculators/scientific-calculators/">Scientific Calculators</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13038"><a href="https://orpatgroup.com/product-category/orpat-fans/">Orpat Fans</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13039"><a href="https://orpatgroup.com/product-category/orpat-fans/economy-fans/">Economy Fans</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13040"><a href="https://orpatgroup.com/product-category/orpat-fans/exhaust-fans/">Exhaust Fans</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13041"><a href="https://orpatgroup.com/product-category/orpat-fans/pedestal-fans/">Pedestal Fans</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13042"><a href="https://orpatgroup.com/product-category/orpat-fans/premium-fans/">Premium Fans</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13043"><a href="https://orpatgroup.com/product-category/orpat-fans/table-fans/">Table Fans</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13044"><a href="https://orpatgroup.com/product-category/orpat-fans/ventilation-fans/">Ventilation Fans</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13045"><a href="https://orpatgroup.com/product-category/orpat-fans/wall-fans/">Wall Fans</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11092"><a href="https://orpatgroup.com/product-category/orpat-switches/">Orpat Switches</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13046"><a href="https://orpatgroup.com/product-category/orpat-switches/platinum-series-fan-regulators/">Platinum Series Fan Regulators</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13047"><a href="https://orpatgroup.com/product-category/orpat-switches/platinum-series-sockets/">Platinum Series Sockets</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13048"><a href="https://orpatgroup.com/product-category/orpat-switches/platinum-series-switches/">Platinum Series Switches</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13049"><a href="https://orpatgroup.com/product-category/orpat-switches/silver-series-cases-covers/">Silver Series Cases &#038; Covers</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13050"><a href="https://orpatgroup.com/product-category/orpat-telephones/">Orpat Telephones</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13051"><a href="https://orpatgroup.com/product-category/orpat-telephones/basic-phones/">Basic Phones</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13052"><a href="https://orpatgroup.com/product-category/orpat-telephones/caller-id-phones/">Caller ID Phones</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7176"><a href="https://orpatgroup.com/product-category/orpat-toys/">Orpat Toys</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7178"><a href="https://orpatgroup.com/product-category/orpat-toys/educational_toys/">Educational Toys</a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13059"><a href="https://orpatgroup.com/corporate-gifting/">Corporate Gifting</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13056"><a href="#">Partner With Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13057"><a href="https://orpatgroup.com/distributorship-in-india/">Distributorship in India</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13058"><a href="https://orpatgroup.com/international-distributorship/">International Distributorship</a></li>
</ul>
</li>
</ul></div>						</div>
				</nav>
					<!--Cart -->				
							<div class="header-cart headercart-block">
																			
								<div class="cart togg">
																	
										<div class="shopping_cart tog" title="View your shopping cart">
										<span class="cart-icon"></span>
											<a class="cart-contents" href="https://orpatgroup.com/cart/" title="View your shopping cart">1</a>
										</div>	
																						<aside id="woocommerce_widget_cart-1" class="widget woocommerce widget_shopping_cart tab_content"><div class="widget_shopping_cart_content"></div></aside>		
								</div>							
										
							</div>		
							<!-- Topbar link -->							
							<div class="topbar-link">
								<span class="topbar-link-toggle"></span>
								 <div class="topbar-link-wrapper">   
											<div class="header-menu-links">	
											 					
													<ul id="menu-header-top-links" class="header-menu"><li id="menu-item-13100" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13100"><a href="https://orpatgroup.com/my-account/orders/">My Account</a></li>
<li id="menu-item-7408" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7408"><a href="https://orpatgroup.com/checkout-2/">Checkout</a></li>
<li id="menu-item-7218" class="wishlist menu-item menu-item-type-post_type menu-item-object-page menu-item-7218"><a href="https://orpatgroup.com/wishlist/">Wishlist</a></li>
</ul>																																								<a href="https://orpatgroup.com/wp-login.php?action=logout&#038;redirect_to=https%3A%2F%2Forpatgroup.com%2Fmy-account%2F&#038;_wpnonce=166808813b" >Logout</a>
														  
											</div>			
											</div>
										</div>			
							<!--Search-->
														<div class="header-search">
									<div class="header-toggle"></div>
										<div class="search-overlay"><form role="search" method="get" class="woocommerce-product-search" action="https://orpatgroup.com/">
	<label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
	<input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products&hellip;" value="" name="s" />
	<button type="submit" value="Search">Search</button>
	<input type="hidden" name="post_type" value="product" />
</form>
 	</div>
							</div>
										
						</div>		
 <!-- End header-main -->
</div>	

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144753252-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144753252-1');
</script>


</header>
	<div id="main" class="site-main full-width box-page">
<div class="main_inner">
	<div class="main-content-inner full-width">	
<div id="main-content" class="main-content home-page full-width box-page ">
  <div id="primary" class="content-area">
 
    <div id="content" class="site-content" role="main">
      <article id="post-13558" class="post-13558 page type-page status-publish hentry">
    <div class="entry-content">
    <div class="vc_row wpb_row vc_row-fluid no-fa-icon vc_custom_1570626165418 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<h2 style="text-align: center;">Thank you for submitting your details,</h2>
<h2 style="text-align: center;">will get back to you shortly.</h2>
<p>
</div>
		</div></div><div class="vc_empty_space"   style="height: 30px"><span class="vc_empty_space_inner"></span></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid no-fa-icon vc_custom_1570626165418 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div id="catalog" class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/Home_catalogue.pdf" target="_blank" rel="noopener">Home Appliances Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Switch-Platinum-Catalogue.pdf" target="_blank" rel="noopener">Platinum Switches Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Telephone-Catalogue.pdf" target="_blank" rel="noopener">Telephone Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/PALLETS_CAT_2017_18_OK.pdf" target="_blank" rel="noopener">Pallets Catalogue</a></p>
<p>
</div>
		</div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/clock_catalogue_2018_19.pdf" target="_blank" rel="noopener">Clock Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Fans-Catalogue.pdf" target="_blank" rel="noopener">Fan Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Timpiece-Catalogue.pdf" target="_blank" rel="noopener">Time Piece (Alarm Clock) Catalogue</a></p>
<p>
</div>
		</div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Switch-Silver-Catalogue.pdf" target="_blank" rel="noopener">Silver &amp; Copper Switches Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Calculator-Catalogue.pdf" target="_blank" rel="noopener">Calculator Catalogue</a></p>
<p>
</div>
		</div></div><div  class="vc_wp_text wpb_content_element"><div class="widget widget_text">			<div class="textwidget"></p>
<p style="text-align: center;"><a href="https://orpatgroup.com/wp-content/uploads/2019/10/New-Crates-Catalogue.pdf" target="_blank" rel="noopener">Crates Catalogue</a></p>
<p>
</div>
		</div></div></div></div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div>
    <div class="inner-container">
	<span class="edit-link"><a class="post-edit-link" href="https://orpatgroup.com/wp-admin/post.php?post=13558&#038;action=edit">Edit</a> <a href="https://orpatgroup.com/wp-admin/post.php?vc_action=vc_inline&#038;post_id=13558&#038;post_type=page" id="vc_load-inline-editor" class="vc_inline-link">Edit with WPBakery Page Builder</a></span>    </div>
    <!-- .inner-container -->
  </div>
  <!-- .entry-content -->
</article><!-- #post-## -->          </div><!-- #content -->
</div><!-- #primary -->
   <div id="secondary" class="left-col">
      <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
    <aside id="search-2" class="widget widget_search"><form role="search" method="get" id="searchform" class="search-form" action="https://orpatgroup.com/" >
    <div><label class="screen-reader-text" for="s">Search for:</label>
    <input class="search-field" type="text" placeholder="Search" value="" name="s" id="s" />
    <input class="search-submit" type="submit" id="searchsubmit" value="Go" />
    </div>
    </form></aside>		<aside id="recent-posts-2" class="widget widget_recent_entries">		<h3 class="widget-title">Recent Posts</h3>		<ul>
											<li>
					<a href="https://orpatgroup.com/2018/04/02/guide-to-safety-measures-while-handling-and-storing-goods-on-pallets/">Guide to safety measures while handling and storing goods on pallets</a>
									</li>
											<li>
					<a href="https://orpatgroup.com/2018/04/02/a-guide-on-how-to-iron-different-types-of-fabric-clothes/">A Guide on how to iron different types of fabric clothes</a>
									</li>
											<li>
					<a href="https://orpatgroup.com/2018/04/02/five-ways-in-which-using-an-alarm-clock-makes-you-more-productive/">Five ways in which using an alarm clock makes you more productive</a>
									</li>
					</ul>
		</aside><aside id="recent-comments-2" class="widget widget_recent_comments"><h3 class="widget-title">Recent Comments</h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a href='https://wordpress.org/' rel='external nofollow' class='url'>A WordPress Commenter</a></span> on <a href="https://orpatgroup.com/2018/04/02/a-guide-on-how-to-iron-different-types-of-fabric-clothes/#comment-14">A Guide on how to iron different types of fabric clothes</a></li></ul></aside><aside id="archives-2" class="widget widget_archive"><h3 class="widget-title">Archives</h3>		<ul>
			<li><a href='https://orpatgroup.com/2018/04/'>April 2018</a></li>
		</ul>
		</aside><aside id="categories-2" class="widget widget_categories"><h3 class="widget-title">Categories</h3>		<ul>
	<li class="cat-item cat-item-16"><a href="https://orpatgroup.com/category/business/" >Business</a>
</li>
	<li class="cat-item cat-item-20"><a href="https://orpatgroup.com/category/sport/" >Sport</a>
</li>
	<li class="cat-item cat-item-21"><a href="https://orpatgroup.com/category/travel/" >Travel</a>
</li>
	<li class="cat-item cat-item-22"><a href="https://orpatgroup.com/category/westdeal/" >WestDeal</a>
</li>
		</ul>
</aside><aside id="meta-2" class="widget widget_meta"><h3 class="widget-title">Meta</h3>			<ul>
			<li><a rel="nofollow" href="https://orpatgroup.com/wp-admin/">Site Admin</a></li>			<li><a rel="nofollow" href="https://orpatgroup.com/wp-login.php?action=logout&#038;_wpnonce=166808813b">Log out</a></li>
			<li><a href="https://orpatgroup.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="https://orpatgroup.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li>			</ul>
			</aside>		<aside id="recent-posts-3" class="widget widget_recent_entries">		<h3 class="widget-title">Recent Posts</h3>		<ul>
											<li>
					<a href="https://orpatgroup.com/2018/04/02/guide-to-safety-measures-while-handling-and-storing-goods-on-pallets/">Guide to safety measures while handling and storing goods on pallets</a>
									</li>
											<li>
					<a href="https://orpatgroup.com/2018/04/02/a-guide-on-how-to-iron-different-types-of-fabric-clothes/">A Guide on how to iron different types of fabric clothes</a>
									</li>
											<li>
					<a href="https://orpatgroup.com/2018/04/02/five-ways-in-which-using-an-alarm-clock-makes-you-more-productive/">Five ways in which using an alarm clock makes you more productive</a>
									</li>
					</ul>
		</aside>  </div>
  <!-- #primary-sidebar -->
  </div><!-- #secondary --><!-- #main-content -->
</div>
</div>
<!-- .main-content-inner -->
</div>
<!-- .main_inner -->
</div>
<!-- #main -->
<footer id="colophon" class="site-footer">	
	 <div class="middle-container">
		 		<div class="footer-top">
<div id="footer-widget-area">
	  		  <div id="first" class="first-widget footer-widget">
			<aside id="staticlinkswidget-1" class="widget widgets-static-links"><h3 class="widget-title">Information</h3> 
		<ul class="toggle-block">
			<li>
		<div class="static-links-list">
						<span><a href="https://orpatgroup.com/about-us/">
				About Us</a></span>
				
						<span><a href="https://orpatgroup.com/my-account/orders/">
				My Account</a></span>
				
						<span><a href="https://orpatgroup.com/terms-and-conditions/">
				Terms &amp; Conditions</a></span>
				
						<span><a href="https://orpatgroup.com/privacy-policy/">
				Privacy Policy</a></span>
									<span><a href="https://orpatgroup.com/orpat-in-the-news/">
				Orpat In The News
</a></span>
									<span><a href="https://orpatgroup.com/product-videos/">
				Product Videos</a></span>
									<span><a href="https://orpatgroup.com/product-catalogue/">
				Product Catalogue</a></span>
								</div>
		</li>	
		</ul>
		</aside>		  </div>
	  	  		  <div id="second" class="second-widget footer-widget">
			<aside id="staticlinkswidget-2" class="widget widgets-static-links"><h3 class="widget-title">Additional Links</h3> 
		<ul class="toggle-block">
			<li>
		<div class="static-links-list">
						<span><a href="https://orpatgroup.com/fascinating-facts/">
				Fascinating Facts</a></span>
				
						<span><a href="https://orpatgroup.com/social-responsibility/">
				Social Responsibility</a></span>
				
						<span><a href="https://orpatgroup.com/careers-at-orpat/">
				Careers At Orpat</a></span>
				
						<span><a href="https://orpatgroup.com/corporate-gifting/">
				Corporate Gifting</a></span>
									<span><a href="https://orpatgroup.com/distributorship-in-india/">
				Distributorship In India</a></span>
									<span><a href="https://orpatgroup.com/international-distributorship/">
				International Distributorship </a></span>
									<span><a href="https://orpatgroup.com/factory-setup/">
				Factory Setup</a></span>
								</div>
		</li>	
		</ul>
		</aside>		  </div>
	  	  		  <div id="third" class="third-widget footer-widget">
			<aside id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><h3 class="widget-title">Contact Us</h3>
Ajanta LLP<br>
Orpat Industrial Estate, Rajkot Highway,<br>
Morbi – 363641. (Gujarat – India)
<br>
<i class="fa fa-phone" aria-hidden="true"></i> :  +91 – 02822 – 230491</div></aside>		  </div>
	   	    		  <div id="fourth" class="fourth-widget footer-widget">
			<aside id="newsletterwidgetminimal-2" class="widget widget_newsletterwidgetminimal"><h3 class="widget-title">Subscribe to our newsletter</h3><div class="tnp tnp-widget-minimal"><form class="tnp-form" action="https://orpatgroup.com/?na=s" method="post" onsubmit="return newsletter_check(this)"><input type="hidden" name="nr" value="widget-minimal"/><input class="tnp-email" type="email" required name="ne" value="" placeholder="Email"><input class="tnp-submit" type="submit" value="Subscribe"></form></div></aside><aside id="media_image-2" class="widget widget_media_image"><img width="250" height="116" src="https://orpatgroup.com/wp-content/uploads/2019/10/image.png" class="image wp-image-11794  attachment-full size-full" alt="ajanta logo" style="max-width: 100%; height: auto;" /></aside>		  </div>
	  </div></div>	 <div class="footer-bottom">	
				    
				<div class="footer-menu-links">
				<ul id="menu-footermenu" class="footer-menu"><li id="menu-item-13084" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-13084"><a href="https://orpatgroup.com/">Home</a></li>
<li id="menu-item-13085" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13085"><a href="https://orpatgroup.com/about-us/">About Us</a></li>
<li id="menu-item-13086" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13086"><a href="https://orpatgroup.com/product-category/ajanta-clocks/">Ajanta Clocks</a></li>
<li id="menu-item-13087" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13087"><a href="https://orpatgroup.com/product-category/home-appliances/steam-irons/">Irons</a></li>
<li id="menu-item-13088" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13088"><a href="https://orpatgroup.com/product-category/home-appliances/kettles/">Kettles</a></li>
<li id="menu-item-13089" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13089"><a href="https://orpatgroup.com/product-category/home-appliances/choppers/">Choppers</a></li>
<li id="menu-item-13090" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13090"><a href="https://orpatgroup.com/product-category/home-appliances/hand-blenders/">Hand Blenders</a></li>
<li id="menu-item-13091" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13091"><a href="https://orpatgroup.com/product-category/home-appliances/oil-heaters/">Oil Heaters</a></li>
<li id="menu-item-13093" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13093"><a href="https://orpatgroup.com/product-category/orpat-calculators/">Orpat Calculators</a></li>
<li id="menu-item-13095" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13095"><a href="https://orpatgroup.com/product-category/orpat-fans/">Orpat Fans</a></li>
<li id="menu-item-13096" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13096"><a href="https://orpatgroup.com/product-category/orpat-switches/">Orpat Switches</a></li>
<li id="menu-item-13097" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13097"><a href="https://orpatgroup.com/product-category/orpat-telephones/">Orpat Telephones</a></li>
<li id="menu-item-13092" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13092"><a href="https://orpatgroup.com/product-category/orpat-timepieces/">Orpat Timepieces</a></li>
<li id="menu-item-13094" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13094"><a href="https://orpatgroup.com/product-category/orpat-toys/">Orpat Toys</a></li>
</ul>				</div><!-- #footer-menu-links -->	
				 
			   <div class="site-info">  Copyright &copy; 2019 Orpat Group											  </div>
							<aside id="accepted_payment_methods-1" class="widget widget_accepted_payment_methods"><h3 class="widget-title">Accepted Payment Methods</h3><ul class="accepted-payment-methods"><li class="american-express"><span>American Express</span></li><li class="maestro"><span>Maestro</span></li><li class="mastercard"><span>MasterCard</span></li><li class="paypal"><span>PayPal</span></li><li class="visa"><span>Visa</span></li></ul></aside>						</div>
		</div>
</footer>
<!-- #colophon -->
</div>
<!-- #page -->
<div class="backtotop"><a id="to_top" href="#"></a></div>

<div id="yith-quick-view-modal">

	<div class="yith-quick-view-overlay"></div>

	<div class="yith-wcqv-wrapper">

		<div class="yith-wcqv-main">

			<div class="yith-wcqv-head">
				<a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
			</div>

			<div id="yith-quick-view-content" class="woocommerce single-product"></div>

		</div>

	</div>

</div>	<script>
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" aria-label="Close (Esc)"></button>
				<button class="pswp__button pswp__button--share" aria-label="Share"></button>
				<button class="pswp__button pswp__button--fs" aria-label="Toggle fullscreen"></button>
				<button class="pswp__button pswp__button--zoom" aria-label="Zoom in/out"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div>
			</div>
			<button class="pswp__button pswp__button--arrow--left" aria-label="Previous (arrow left)"></button>
			<button class="pswp__button pswp__button--arrow--right" aria-label="Next (arrow right)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/template" id="tmpl-variation-template">
	<div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
	<div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
	<div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
	<p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<link rel='stylesheet' id='woof_tooltip-css-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/tooltip/css/tooltipster.bundle.min.css?ver=1.2.3' media='all' />
<link rel='stylesheet' id='woof_tooltip-css-noir-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/tooltip/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-noir.min.css?ver=1.2.3' media='all' />
<link rel='stylesheet' id='photoswipe-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/css/photoswipe/photoswipe.css?ver=3.7.0' media='all' />
<link rel='stylesheet' id='photoswipe-default-skin-css'  href='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/css/photoswipe/default-skin/default-skin.css?ver=3.7.0' media='all' />
<link rel='stylesheet' id='tmpmela-responsive-css'  href='https://orpatgroup.com/wp-content/themes/cartfolio/responsive.css?ver=4.9.12' media='all' />
<script src='https://orpatgroup.com/wp-includes/js/admin-bar.min.js?ver=4.9.12'></script>
<script>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/orpatgroup.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.7.0'></script>
<script>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_5ceedda9d2213a7f8ad0a1ed12d6a9e9","fragment_name":"wc_fragments_5ceedda9d2213a7f8ad0a1ed12d6a9e9","request_timeout":"5000"};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.7.0'></script>
<script>
/* <![CDATA[ */
var options = {"enable_message":null,"message_text":null};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/wphobby-pdf-invoices-for-woocommerce/assets/js/frontend.js?ver=1.0.2'></script>
<script>
/* <![CDATA[ */
var yith_woocompare = {"ajaxurl":"\/?wc-ajax=%%endpoint%%","actionadd":"yith-woocompare-add-product","actionremove":"yith-woocompare-remove-product","actionview":"yith-woocompare-view-table","actionreload":"yith-woocompare-reload-product","added_label":"Added","table_title":"Product Comparison","auto_open":"yes","loader":"https:\/\/orpatgroup.com\/wp-content\/plugins\/yith-woocommerce-compare\/assets\/images\/loader.gif","button_text":"Compare","cookie_name":"yith_woocompare_list","close_label":"Close"};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-compare/assets/js/woocompare.min.js?ver=2.3.13'></script>
<script src='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-compare/assets/js/jquery.colorbox-min.js?ver=1.4.21'></script>
<script>
/* <![CDATA[ */
var yith_qv = {"ajaxurl":"\/wp-admin\/admin-ajax.php","loader":"https:\/\/orpatgroup.com\/wp-content\/plugins\/yith-woocommerce-quick-view\/assets\/image\/qv-loader.gif","is2_2":"","lang":""};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-quick-view/assets/js/frontend.min.js?ver=1.3.13'></script>
<script src='//orpatgroup.com/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6'></script>
<script src='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0'></script>
<script>
/* <![CDATA[ */
var yith_wcwl_l10n = {"ajax_url":"\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","is_user_logged_in":"1","ajax_loader_url":"https:\/\/orpatgroup.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif","remove_from_wishlist_after_add_to_cart":"yes","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser.","added_to_cart_message":"<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","move_to_another_wishlist_action":"move_to_another_wishlsit","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem"}};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.2.13'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/functions.js?ver=2014-02-01'></script>
<script src='https://orpatgroup.com/wp-content/themes/cartfolio/js/navigation.js?ver=1.0'></script>
<script src='https://orpatgroup.com/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=6.0.3'></script>
<script>
/* <![CDATA[ */
var newsletter = {"messages":{"email_error":"Email address is not correct","name_error":"Name is required","surname_error":"Last name is required","profile_error":"A mandatory field is not filled in","privacy_error":"You must accept the privacy policy"},"profile_max":"20"};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/newsletter/subscription/validate.js?ver=6.2.2'></script>
<script src='https://orpatgroup.com/wp-includes/js/wp-embed.min.js?ver=4.9.12'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/tooltip/js/tooltipster.bundle.min.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/front.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/html_types/radio.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/html_types/checkbox.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/html_types/select.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/html_types/mselect.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/chosen/chosen.jquery.min.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce-products-filter/js/plainoverlay/jquery.plainoverlay.min.js?ver=1.2.3'></script>
<script src='https://orpatgroup.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.3'></script>
<script src='https://orpatgroup.com/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-includes/js/wp-util.min.js?ver=4.9.12'></script>
<script>
/* <![CDATA[ */
var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=3.7.0'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/zoom/jquery.zoom.min.js?ver=1.7.21'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe.min.js?ver=4.1.1'></script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min.js?ver=4.1.1'></script>
<script>
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":"1"};
/* ]]> */
</script>
<script src='https://orpatgroup.com/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=3.7.0'></script>
		<script>	
			jQuery(document).ready(function() {			
				
				if (jQuery('#wp-admin-bar-revslider-default').length>0 && jQuery('.rev_slider_wrapper').length>0) {
					var aliases = new Array();
					jQuery('.rev_slider_wrapper').each(function() {
						aliases.push(jQuery(this).data('alias'));
					});								
					if(aliases.length>0)
						jQuery('#wp-admin-bar-revslider-default li').each(function() {
							var li = jQuery(this),
								t = jQuery.trim(li.find('.ab-item .rs-label').data('alias')); //text()
								
							if (jQuery.inArray(t,aliases)!=-1) {
							} else {
								li.remove();
							}
						});
				} else {
					jQuery('#wp-admin-bar-revslider').remove();
				}
			});
		</script>
		
                <style>
        
        


        



        
        </style>
        

        	<!--[if lte IE 8]>
		<script>
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script>
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
		

		
<!-- freshchat code--->
<script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "da9aeda2-04c7-4831-bbbc-d801077d54da",
      host: "https://wchat.freshchat.com"
    });
  }
  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>


</body></html>