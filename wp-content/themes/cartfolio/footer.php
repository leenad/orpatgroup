<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage TemplateMela
 * @since TemplateMela 1.0
 */
?>
<?php tmpmela_content_after(); ?>
</div>
<!-- .main-content-inner -->
</div>
<!-- .main_inner -->
</div>
<!-- #main -->
<?php tmpmela_footer_before(); ?>
<footer id="colophon" class="site-footer">	
	 <div class="middle-container">
		 <?php tmpmela_footer_inside(); ?>
		<?php get_sidebar('footer'); ?>
	 <div class="footer-bottom">	
				<?php if ( has_nav_menu('footer-menu') ) { ?>    
				<div class="footer-menu-links">
				<?php
							$tmpmela_footer_menu=array(
							'menu' => esc_html__('TM Footer Navigation','cartfolio'),
							'depth'=> 1,
							'echo' => false,
							'menu_class'      => 'footer-menu', 
							'container'       => '', 
							'container_class' => '', 
							'theme_location' => 'footer-menu'
							);
							echo wp_nav_menu($tmpmela_footer_menu);				    
							?>
				</div><!-- #footer-menu-links -->	
				<?php } ?> 
			   <div class="site-info">  <?php echo esc_html__( 'Copyright', 'cartfolio' ); ?> &copy; <?php echo esc_attr(date('Y')); ?> <?php echo esc_attr(stripslashes(get_option('tmpmela_footer_slog')));?>
						<?php do_action( 'tmpmela_credits' ); ?>
					  </div>
			<?php if ( is_active_sidebar( 'footer-bottom-widget-area' ) ) : ?>
				<?php dynamic_sidebar( 'footer-bottom-widget-area' ); ?>
			<?php endif; ?>
			</div>
		</div>
</footer>
<!-- #colophon -->
<?php tmpmela_footer_after(); ?>
</div>
<!-- #page -->
<?php tmpmela_go_top(); ?>
<?php tmpmela_get_widget('before-end-body-widget'); ?>
<?php wp_footer(); ?>

<!-- freshchat code--->
<!-- <script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "da9aeda2-04c7-4831-bbbc-d801077d54da",
      host: "https://wchat.freshchat.com"
    });
  }
  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script> -->

<!-- <script>
	$('a.mega-hdr-a').click(function() {
	    $('li.cat-parent').removeClass('expandable');
	    $(this).addClass('collapsable');
	});
</script> -->

<!-- <script>
	$('a.mega-hdr-a').click(function() {
	    $('li.cat-parent').removeClass('expandable');
	    $(this).addClass('collapsable');
	});
</script> -->

</body></html>